import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import SidebarPengelola from './Pengelola';

class SidebarWadir extends Component {
    render() {
        return (
            <div>
                <SidebarPengelola/>
                
                {/* Data */}
                <div className="sidebar-heading">
                    KELOLA DATA AKUN
                </div>
                
                {/* Data Mahasiswa */}
                <li className="nav-item">
                    <Link className="nav-link collapsed" data-toggle="collapse" data-target="#collapseDataMahasiswa" aria-expanded="true" aria-controls="collapseDataMahasiswa">
                        <i className="fas fa-id-card"></i>
                        <span>Data Mahasiswa Asrama</span>
                    </Link>
                    <div id="collapseDataMahasiswa" className="collapse" aria-labelledby="headingDataMahasiswa" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <h6 className="collapse-header">Menu:</h6>
                            <Link className="collapse-item" to="/import-mahasiswa">Import Data</Link>
                            <Link className="collapse-item" to="/data-mahasiswa">Daftar Mahasiswa</Link>
                            <Link className="collapse-item" to="/arsip-mahasiswa">Arsip Mahasiswa</Link>
                        </div>
                    </div>
                </li>

                {/* Data Pengelola */}
                <li className="nav-item">
                    <Link className="nav-link collapsed" data-toggle="collapse" data-target="#collapseDataPengelola" aria-expanded="true" aria-controls="collapseDataPengelola">
                        <i className="fas fa-id-card"></i>
                        <span>Data Pengelola Asrama</span>
                    </Link>
                    <div id="collapseDataPengelola" className="collapse" aria-labelledby="headingDataPengelola" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <h6 className="collapse-header">Menu:</h6>
                            <Link className="collapse-item" to="/data-pengelola">Daftar Pengelola</Link>
                        </div>
                    </div>
                </li>

                {/* <!-- Divider --> */}
                <hr className="sidebar-divider" />
            </div>
        );
    }
}

export default SidebarWadir;