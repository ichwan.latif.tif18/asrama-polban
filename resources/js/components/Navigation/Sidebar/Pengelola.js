import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class SidebarPengelola extends Component {
    render() {
        return (
            <div>
                <div className="sidebar-heading">
                    PRESENSI DAN PERIZINAN
                </div>

                {/* Presensi */}
                <li className="nav-item">
                    <Link className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePresensi" aria-expanded="true" aria-controls="collapsePresensi">
                        <i className="fas fa-clipboard-check"></i>
                        <span>Presensi</span>
                    </Link>
                    <div id="collapsePresensi" className="collapse" aria-labelledby="headingPresensi" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <h6 className="collapse-header">Kelola Presensi:</h6>
                            <Link className="collapse-item" to="/jadwal-presensi">Atur Jadwal Presensi</Link>
                            <Link className="collapse-item" to="/data-presensi">Presensi Harian Asrama</Link>
                            <Link className="collapse-item" to="/rekapitulasi">Rekap Data Presensi</Link>
                        </div>
                    </div>
                </li>

                {/* Form Approval */}
                <li className="nav-item">
                    <Link className="nav-link collapsed" data-toggle="collapse" data-target="#collapsePerizinan" aria-expanded="true" aria-controls="collapsePerizinan">
                        <i className="fas fa-clipboard-check"></i>
                        <span>Perizinan</span>
                    </Link>
                    <div id="collapsePerizinan" className="collapse" aria-labelledby="headingPerizinan" data-parent="#accordionSidebar">
                        <div className="bg-white py-2 collapse-inner rounded">
                            <h6 className="collapse-header">Jenis Perizinan:</h6>
                            <Link className="collapse-item" to="/data-izin-pulang">Izin Pulang Asrama</Link>
                            <Link className="collapse-item" to="/data-izin-kembali">Izin Kembali Asrama</Link>
                            <Link className="collapse-item" to="/data-resign">Resign Asrama</Link>
                        </div>
                    </div>
                </li>

                {/* <!-- Divider --> */}
                <hr className="sidebar-divider" />
            </div>
        );
    }
}

export default SidebarPengelola;