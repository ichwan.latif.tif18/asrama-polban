import React, { Component } from 'react';
import Chart from "chart.js";

import CardBasic from '../../Cards/Basic';

class ChartDonut extends Component {

    chartRef = React.createRef();

    componentDidMount() {
        console.log(this.chartRef);

        new Chart(this.props.title, {
            type: 'doughnut',
            data: {
                labels: [this.props.label1, this.props.label2, this.props.label3],
                datasets: [{
                    data: [this.props.data1, this.props.data2, this.props.data3],
                    backgroundColor: ['#1cca8d', '#f8d16d', '#eb6c60'],
                    hoverBackgroundColor: ['#17a673', '#f6c23e', '#e74a3b'],
                    hoverBorderColor: "rgba(234, 236, 244, 1)",
                }],
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    caretPadding: 10,
                },
                legend: {
                    display: false
                },
                cutoutPercentage: 80,
            },
        });
    }

    componentDidUpdate(prevProps) {
        if (this.props !== prevProps) {
            new Chart(this.props.title, {
                type: 'doughnut',
                data: {
                    labels: [this.props.label1, this.props.label2, this.props.label3],
                    datasets: [{
                        data: [this.props.data1, this.props.data2, this.props.data3],
                        backgroundColor: ['#1cca8d', '#f8d16d', '#eb6c60'],
                        hoverBackgroundColor: ['#17a673', '#f6c23e', '#e74a3b'],
                        hoverBorderColor: "rgba(234, 236, 244, 1)",
                    }],
                },
                options: {
                    maintainAspectRatio: false,
                    tooltips: {
                        backgroundColor: "rgb(255,255,255)",
                        bodyFontColor: "#858796",
                        borderColor: '#dddfeb',
                        borderWidth: 1,
                        xPadding: 15,
                        yPadding: 15,
                        displayColors: false,
                        caretPadding: 10,
                    },
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 80,
                },
            });
        }
      }

    render() {
        return (
            <CardBasic title={this.props.title}>
                 <div className="chart-pie pt-4">
                        <canvas id={this.props.title} ref={this.chartRef}></canvas>
                    </div>
                    <div className="mt-4 text-center small">
                        <span className="mr-2">
                            <i className="fas fa-circle text-success"></i> {this.props.label1}
                        </span>
                        <span className="mr-2">
                            <i className="fas fa-circle text-warning"></i> {this.props.label2}
                        </span>
                        <span className="mr-2">
                            <i className="fas fa-circle text-danger"></i> {this.props.label3}
                        </span>
                    </div>
            </CardBasic>
        )
    }
}

export default ChartDonut;