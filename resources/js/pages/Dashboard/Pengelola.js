import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import CardInfo from '../../components/Cards/Info';
import CardBasic from '../../components/Cards/Basic';
import ChartDonut from '../../components/Charts/Donut';
import PageHeading from '../../components/PageHeading';
import api from '../../service/api';

class DashboarPengelola extends Component {
    constructor(){
        super();
        this.state = {
            alfaA: 0,
            hadirA: 0,
            izinA: 0,
            mhsA: 0,
            pengurusA: 0,

            alfaB: 0,
            hadirB: 0,
            izinB: 0,
            mhsB: 0,
            pengurusB: 0,

            alfaC: 0,
            hadirC: 0,
            izinC: 0,
            mhsC: 0,
            pengurusC: 0,
        }
    }

    async componentDidMount(){
        await api().get('api/presensi/dashboard').then(response =>{
            if(response.data.status == 'success'){
                this.setState({
                    alfaA: response.data.gedungA[0],
                    hadirA: response.data.gedungA[1],
                    izinA: response.data.gedungA[2],
                    mhsA: response.data.gedungA[3],
                    pengurusA: response.data.gedungA[4],

                    alfaB: response.data.gedungB[0],
                    hadirB: response.data.gedungB[1],
                    izinB: response.data.gedungB[2],
                    mhsB: response.data.gedungB[3],
                    pengurusB: response.data.gedungB[4],

                    alfaC: response.data.gedungC[0],
                    hadirC: response.data.gedungC[1],
                    izinC: response.data.gedungC[2],
                    mhsC: response.data.gedungC[3],
                    pengurusC: response.data.gedungC[4],

                    mengajukanA: 0,
                    disetujuiA: 0,
                    ditolakA: 0,

                    mengajukanB: 0,
                    disetujuiB: 0,
                    ditolakB: 0,

                    mengajukanC: 0,
                    disetujuiC: 0,
                    ditolakC: 0,
                })
                console.log(response.data)
            }
            else{
                alert(response.data.message);
            }
        })

        api().get('api/perizinan/dashboard/3').then(response =>{
            if(response.data.status === 'success'){
                this.setState({
                    mengajukanA: response.data.gedungA[0],
                    disetujuiA: response.data.gedungA[1],
                    ditolakA: response.data.gedungA[2],

                    mengajukanB: response.data.gedungB[0],
                    disetujuiB: response.data.gedungB[1],
                    ditolakB: response.data.gedungB[2],

                    mengajukanC: response.data.gedungC[0],
                    disetujuiC: response.data.gedungC[1],
                    ditolakC: response.data.gedungC[2],
                })
                console.log(response.data)
            }
            else{
                alert(response.data.message);
            }
        })
    }

    render() {
        let presensiA;
        let perizinanA;
        let presensiB;
        let perizinanB;
        let presensiC;
        let perizinanC;
        // Asrama A
        if(this.state.hadirA || this.state.izinA || this.state.alfaA){
            presensiA =<ChartDonut title="Presensi  Asrama A"
                    data1={this.state.hadirA}
                    data2={this.state.izinA}
                    data3={this.state.alfaA}
                    label1="Hadir"
                    label2="Izin"
                    label3="Alpa"
                    />
        }else{
            presensiA =<CardBasic title="Presensi  Asrama A">
               <div className="text-center">Tidak ada data presensi</div>
            </CardBasic>
        }

        if(this.state.disetujuiA || this.state.mengajukanA || this.state.ditolakA){
            perizinanA = <ChartDonut title="Perizinan  Asrama A"
                data1={this.state.disetujuiA}
                data2={this.state.mengajukanA}
                data3={this.state.ditolakA}
                label1="Disetujui"
                label2="Mengajukan"
                label3="Ditolak"
                />
        }else{
            perizinanA =<CardBasic title="Perizinan  Asrama A">
               <div className="text-center">Tidak ada data perizinan</div>
            </CardBasic>
        }
        // Asrama B
        if(this.state.hadirB || this.state.izinB || this.state.alfaB){
            presensiB =<ChartDonut title="Presensi  Asrama B"
                    data1={this.state.hadirB}
                    data2={this.state.izinB}
                    data3={this.state.alfaB}
                    label1="Hadir"
                    label2="Izin"
                    label3="Alpa"
                    />
        }else{
            presensiB =<CardBasic title="Presensi  Asrama B">
               <div className="text-center">Tidak ada data presensi</div>
            </CardBasic>
        }

        if(this.state.disetujuiB || this.state.mengajukanB || this.state.ditolakB){
            perizinanB = <ChartDonut title="Perizinan  Asrama B"
                data1={this.state.disetujuiB}
                data2={this.state.mengajukanB}
                data3={this.state.ditolakB}
                label1="Disetujui"
                label2="Mengajukan"
                label3="Ditolak"
                />
        }else{
            perizinanB =<CardBasic title="Perizinan  Asrama B">
               <div className="text-center">Tidak ada data perizinan</div>
            </CardBasic>
        }
        // Asrama C
        if(this.state.hadirC || this.state.izinC || this.state.alfaC){
            presensiC =<ChartDonut title="Presensi  Asrama C"
                    data1={this.state.hadirC}
                    data2={this.state.izinC}
                    data3={this.state.alfaC}
                    label1="Hadir"
                    label2="Izin"
                    label3="Alpa"
                    />
        }else{
            presensiC =<CardBasic title="Presensi  Asrama C">
               <div className="text-center">Tidak ada data presensi</div>
            </CardBasic>
        }

        if(this.state.disetujuiC || this.state.mengajukanC || this.state.ditolakC){
            perizinanC = <ChartDonut title="Perizinan  Asrama C"
                data1={this.state.disetujuiC}
                data2={this.state.mengajukanC}
                data3={this.state.ditolakC}
                label1="Disetujui"
                label2="Mengajukan"
                label3="Ditolak"
                />
        }else{
            perizinanC =<CardBasic title="Perizinan  Asrama C">
               <div className="text-center">Tidak ada data perizinan</div>
            </CardBasic>
        }
        
        return (
            <div className="container-fluid">
                <PageHeading title={this.props.title} />

                {/* Path */}
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><Link to="#">Home</Link></li>
                        <li className="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>

                {/* ASRAMA A */}
                <div className="row">
                    <CardInfo title="Gedung Asrama"
                        icon="house-user"
                        color="primary"
                        value="A" />

                    <CardInfo title="Jumlah Mahasiswa"
                        icon="users"
                        color="primary"
                        value={this.state.mhsA} />

                    <CardInfo title="Jumlah Pengurus"
                        icon="users"
                        color="primary"
                        value={this.state.pengurusA} />
                </div>
                <div className="row">
                  <div className="col-xl-6 col-lg-6">
                    {presensiA}
                  </div>
                  <div className="col-xl-6 col-lg-6">
                    {perizinanA}
                  </div>
                </div>
                <hr/>

                {/* ASRAMA B */}
                <div className="row">
                    <CardInfo title="Gedung Asrama"
                        icon="house-user"
                        color="primary"
                        value="B" />

                    <CardInfo title="Jumlah Mahasiswa"
                        icon="users"
                        color="primary"
                        value={this.state.mhsB} />

                    <CardInfo title="Jumlah Pengurus"
                        icon="users"
                        color="primary"
                        value={this.state.pengurusB} />
                </div>
                <div className="row">
                  <div className="col-xl-6 col-lg-6">
                    {presensiB}
                  </div>
                  <div className="col-xl-6 col-lg-6">
                    {perizinanB}
                  </div>
                </div>
                <hr/>

                {/* ASRAMA C */}
                <div className="row">
                    <CardInfo title="Gedung Asrama"
                        icon="house-user"
                        color="primary"
                        value="C" />

                    <CardInfo title="Jumlah Mahasiswa"
                        icon="users"
                        color="primary"
                        value={this.state.mhsC} />

                    <CardInfo title="Jumlah Pengurus"
                        icon="users"
                        color="primary"
                        value={this.state.pengurusC} />
                </div>
                <div className="row">
                  <div className="col-xl-6 col-lg-6">
                    {presensiC}
                  </div>
                  <div className="col-xl-6 col-lg-6">
                    {perizinanC}
                  </div>
                </div>

            </div>
        );
    }
}

export default DashboarPengelola;