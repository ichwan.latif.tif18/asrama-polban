import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Navigation
import Sidebar from '../../components/Navigation/Sidebar';
import Topbar from '../../components/Navigation/Topbar';
import Footer from '../../components/Navigation/Footer';
import Pagination from "react-js-pagination";

import PageHeading from '../../components/PageHeading';

import api from '../../service/api';

class DataPengelola extends Component {
    constructor(){
        super();
        this.state = {
            role: "",
            datas: [],

            // pagination
            currentData: [],
            activePage: 1,
            itemPerPage : 10,

            // search
            searchData: [],
            searchValue :"",

            // delete
            checkedBoxes: [],
        };
        this.handleSearchChange = this.handleSearchChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount(){
        this.setState({
            role: localStorage.getItem("user_role")
        });

        api().get('api/pengelola').then(response =>{
            if(response.data.status === 'success'){
                this.setState({
                    datas: response.data.data
                })
                console.log(this.state.datas)
            }
            else{
                alert(response.data.message);
            }
        })
    }

    handleSearchChange(e){
        console.log(`search key is ${e.target.value}`);
        this.setState({searchValue: e.target.value});

        const searchData =
            this.state.datas.filter(pengelola => {
                    return pengelola.nama_pengelola.toLowerCase().includes(this.state.searchValue.toLowerCase())
            }
        )
        this.setState( {searchData} );
        this.setState({activePage: 1});
    }

    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage: pageNumber});

        const data = this.state.searchData;
        const offset = (this.state.activePage - 1) * this.state.itemPerPage;
        const currentData = data.slice(offset, offset + this.state.itemPerPage);

        this.setState({ currentData });
    }

    toggleCheckbox(e, item){
        let arr = this.state.checkedBoxes;		
        if(e.target.checked) {
            arr.push(item.id_pengelola);
            this.setState({ checkedBoxes : arr });
        } else {			
            arr.splice(this.state.checkedBoxes.indexOf(item.id_pengelola), 1);
            this.setState({ checkedBoxes: arr });
        }	
        console.log("Delete count: "+this.state.checkedBoxes.length);
    }

    async handleDelete(e){
        if (confirm("Yakin untuk menghapus pengelola?")) {
            e.preventDefault();
            console.log(this.state.checkedBoxes);
            await api().post('api/pengelola/delete', ({
                listPengelola: this.state.checkedBoxes,
            })).then(response => {
                if(response.data.status == 'success'){
                    alert(response.data.message);
                    window.location.reload();
                }
                else{
                    alert(response.data.message);
                }
            })
        }
    }

    render() {
        //search
        const searchData =
            this.state.datas.filter(pengelola => {
                    return pengelola.nama_pengelola.toLowerCase().includes(this.state.searchValue.toLowerCase())
            }
        )

        // pagination
        const offset = (this.state.activePage - 1) * this.state.itemPerPage;
        const currentData = searchData.slice(offset, offset + this.state.itemPerPage);

        let TableStatus;
        if (this.state.datas.length == 0) {
            TableStatus = <h6 className="text-center">Tidak ada data pengelola</h6>;
          } else {
            TableStatus = <div className="form-inline"><label for="itemPerPage">Jumlah per halaman</label>
                            <div className="mb-2 mx-2">
                                <select name="itemPerPage" id="itemPerPage" className="form-control" onChange={this.handleFilterChange}>
                                    <option value={10}>10</option>
                                    <option value={25}>25</option>
                                    <option value={50}>50</option>
                                    <option value={100}>100</option>
                                </select>
                            </div></div>;
        }

        return (
            <div>
                <div id="wrapper">
                {/* <!-- Sidebar --> */}
                <Sidebar role= {this.state.role} />
                {/* <!-- End of Sidebar --> */}
                    <div id="content-wrapper" className="d-flex flex-column">
                        <div id="content">
                        {/* <!-- Topbar --> */}
                        <Topbar />
                        {/* <!-- End of Topbar --> */}
                        <div className="container-fluid">
                            <PageHeading title="Data Pengelola Asrama" />

                            {/* Path */}
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to="#">Home</Link></li>
                                    <li className="breadcrumb-item active" aria-current="page">Data Pengelola</li>
                                </ol>
                            </nav>

                            <div className="col-lg-12 col-md-12">
                                <div className="card my-5">
                                    <div className="card-header">
                                        <h6 className="text-primary">Data Pengelola Asrama</h6>
                                    </div>
                                    <div className="card-body">

                                        <div className="row">
                                            {/* Hapus pengelola */}
                                            <button onClick={this.handleDelete} className="btn btn-danger btn-icon-split mb-2 mx-2">
                                                <span className="icon text-white-50">
                                                    <i className="fas fa-trash-alt"></i>
                                                </span>
                                                <span className="text">Hapus pengelola({this.state.checkedBoxes.length})</span>
                                            </button>

                                            {/* Tambah pengelola */}
                                            <Link to="/tambah-pengelola" className="btn btn-primary btn-icon-split mb-2 mx-2">
                                                <span className="icon text-white-50">
                                                    <i className="fas fa-user-plus"></i>
                                                </span>
                                                <span className="text">Tambah pengelola</span>
                                            </Link>
                                        </div>

                                        {/* Search Bar */}
                                        <div className="input-group mb-2 border rounded-pill p-1 col-lg-4 col-md-8 col-sm-12">
                                        <input type="text" name="searchValue" placeholder="Cari pengelola.." className="form-control bg-none border-0" onChange={this.handleSearchChange}/>
                                            <div className="input-group-append border-0">
                                                <button type="submit" className="btn btn-link text-primary"><i className="fa fa-search"></i></button>
                                            </div>
                                        </div>

                                        {/* Tabel pengelola */}
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th scope="col"></th>
                                                        <th scope="col">Nama</th>
                                                        <th scope="col">Jenis kelamin</th>
                                                        <th scope="col">Email</th>
                                                        <th scope="col">Nomor handphone</th>
                                                        <th scope="col">Alamat</th>
                                                        <th scope="col"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {currentData.map(function(pengelola, index) {
                                                        let jk;
                                                        if(pengelola.jenis_kelamin === 0){
                                                            jk = "Perempuan"
                                                        }
                                                        else{
                                                            jk = "Laki-laki"
                                                        }
                                                        return (
                                                            <tr key={index}>
                                                                <td>
                                                                    <input 
                                                                        type="checkbox" 
                                                                        value={pengelola.id_pengelola} 
                                                                        checked={this.state.checkedBoxes.includes(pengelola.id_pengelola)?"checked":""} 
                                                                        onChange={(e) => this.toggleCheckbox(e, pengelola)}>
                                                                    </input>
                                                                </td>
                                                                <td className="text-uppercase">{pengelola.nama_pengelola}</td>
                                                                <td>{jk}</td>
                                                                <td>{pengelola.email}</td>
                                                                <td>{pengelola.no_hp_pengelola}</td>
                                                                <td>{pengelola.alamat}</td>
                                                                <td>
                                                                    <Link to={"/edit-pengelola/" + pengelola.id_pengelola} className="text-success mx-1">
                                                                        <i className="fas fa-edit"></i>
                                                                    </Link>
                                                                </td>
                                                            </tr>
                                                        )}.bind(this))
                                                    }
                                                </tbody>
                                            </table>
                                        </div>

                                        {TableStatus}
                                        
                                        {/* pagination */}
                                        <div className="d-flex justify-content-end">
                                            <Pagination
                                                itemClass="page-item"
                                                linkClass="page-link"
                                                activePage={this.state.activePage}
                                                itemsCountPerPage={this.state.itemPerPage}
                                                totalItemsCount={searchData.length}
                                                pageRangeDisplayed={3}
                                                onChange={this.handlePageChange.bind(this)}
                                            />
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- Footer --> */}
                    <Footer/>
                    {/* <!-- End of Footer --> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default DataPengelola;