import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Navigation
import Sidebar from '../../components/Navigation/Sidebar';
import Topbar from '../../components/Navigation/Topbar';
import Footer from '../../components/Navigation/Footer';
import Pagination from "react-js-pagination";

import PageHeading from '../../components/PageHeading';

import api from '../../service/api';

class JadwalPresensi extends Component {
    constructor(){
        super();
        this.state = {
            role: '',
            datas: [],

            // pagination
            currentData: [],
            activePage: 1,
            itemPerPage : 10,

            // delete
            checkedBoxes: [],
        };
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount(){
        this.setState({
            role: localStorage.getItem("user_role")
        });
        api().get('api/jadwal').then(response =>{
            if(response.data.status === 'success'){
                this.setState({
                    datas: response.data.data
                })
                console.log(this.state.datas)
            }
            else{
                alert(response.data.message);
            }
        })
    }

    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage: pageNumber});

        const data = this.state.datas;
        const offset = (this.state.activePage - 1) * this.state.itemPerPage;
        const currentData = data.slice(offset, offset + this.state.itemPerPage);

        this.setState({ currentData });
    }

    toggleCheckbox(e, item){
        let arr = this.state.checkedBoxes;		
        if(e.target.checked) {
            arr.push(item.id_jadwal_presensi);
            this.setState({ checkedBoxes : arr });
        } else {			
            arr.splice(this.state.checkedBoxes.indexOf(item.id_jadwal_presensi), 1);
            this.setState({ checkedBoxes: arr });
        }
        console.log(this.state.checkedBoxes);	
        console.log("Delete count: "+this.state.checkedBoxes.length);
    }

    async handleDelete(e){
        e.preventDefault();
        console.log(this.state.checkedBoxes);
        await api().post('api/jadwal/delete', ({
            listJadwal: this.state.checkedBoxes,
        })).then(response => {
            if(response.data.status == 'success'){
                alert(response.data.message);
                window.location.reload();
            }
            else{
                alert(response.data.message);
            }
        })
    }

    render() {
        const datas = this.state.datas;

        // pagination
        const offset = (this.state.activePage - 1) * this.state.itemPerPage;
        const currentData = datas.slice(offset, offset + this.state.itemPerPage);

        let TableStatus;
        if (this.state.datas.length == 0) {
            TableStatus = <h6 className="text-center">Tidak ada jadwal presensi</h6>;
          } else {
            TableStatus = <div className="form-inline"><label for="itemPerPage">Jumlah per halaman</label>
                            <div className="mb-2 mx-2">
                                <select name="itemPerPage" id="itemPerPage" className="form-control" onChange={this.handleFilterChange}>
                                    <option value={10}>10</option>
                                    <option value={25}>25</option>
                                    <option value={50}>50</option>
                                    <option value={100}>100</option>
                                </select>
                            </div></div>;
        }

        return (
            <div>
                <div id="wrapper">
                {/* <!-- Sidebar --> */}
                <Sidebar role= {this.state.role} />
                {/* <!-- End of Sidebar --> */}
                    <div id="content-wrapper" className="d-flex flex-column">
                        <div id="content">
                        {/* <!-- Topbar --> */}
                        <Topbar />
                        {/* <!-- End of Topbar --> */}
                        <div className="container-fluid">
                            <PageHeading title="Atur Jadwal Presensi" />

                            {/* Path */}
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to="#">Home</Link></li>
                                    <li className="breadcrumb-item active" aria-current="page">Jadwal Presensi</li>
                                </ol>
                            </nav>

                            <div className="col-lg-12 col-md-12">
                                <div className="card my-5">
                                    <div className="card-header">
                                        <h6 className="text-primary">Jadwal Presensi Asrama</h6>
                                    </div>
                                    <div className="card-body">

                                        <div className="row">
                                            {/* Hapus jadwal */}
                                            <button onClick={this.handleDelete} className="btn btn-danger btn-icon-split mb-2 mx-2">
                                                <span className="icon text-white-50">
                                                    <i className="fas fa-trash-alt"></i>
                                                </span>
                                                <span className="text">Hapus jadwal({this.state.checkedBoxes.length})</span>
                                            </button>

                                            {/* Tambah jadwal */}
                                            <Link to="/tambah-jadwal" className="btn btn-primary btn-icon-split mb-2 mx-2">
                                                <span className="icon text-white-50">
                                                    <i className="fas fa-user-plus"></i>
                                                </span>
                                                <span className="text">Tambah jadwal</span>
                                            </Link>
                                        </div>

                                        {/* Tabel jadwal */}
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th scope="col"></th>
                                                        <th scope="col">Nama jadwal</th>
                                                        <th scope="col">Jam mulai</th>
                                                        <th scope="col">Jam selesai</th>
                                                        <th scope="col">Status</th>
                                                        <th scope="col"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                {currentData.map(function(jadwal, index) {
                                                        let status;
                                                        if(jadwal.status_jadwal === 0){
                                                            status = "Tidak aktif"
                                                        }
                                                        else{
                                                            status = "Aktif"
                                                        }
                                                        return (
                                                            <tr key={index}>
                                                                <td>
                                                                    <input 
                                                                        type="checkbox" 
                                                                        value={jadwal.id_jadwal_presensi} 
                                                                        checked={this.state.checkedBoxes.includes(jadwal.id_jadwal_presensi)?"checked":""} 
                                                                        onChange={(e) => this.toggleCheckbox(e, jadwal)}>
                                                                    </input>
                                                                </td>
                                                                <td>{jadwal.nama_jadwal}</td>
                                                                <td>{jadwal.jam_mulai}</td>
                                                                <td>{jadwal.jam_selesai}</td>
                                                                <td>{status}</td>
                                                                <td>
                                                                    <Link to={"/edit-jadwal/" + jadwal.id_jadwal_presensi} className="text-success mx-1">
                                                                        <i className="fas fa-edit"></i>
                                                                    </Link>
                                                                </td>
                                                            </tr>
                                                        )}.bind(this))
                                                    }
                                                </tbody>
                                            </table>
                                        </div>

                                        {TableStatus}
                                        
                                        {/* pagination */}
                                        <div className="d-flex justify-content-end">
                                            <Pagination
                                                itemClass="page-item"
                                                linkClass="page-link"
                                                activePage={this.state.activePage}
                                                itemsCountPerPage={this.state.itemPerPage}
                                                totalItemsCount={datas.length}
                                                pageRangeDisplayed={3}
                                                onChange={this.handlePageChange.bind(this)}
                                            />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- Footer --> */}
                    <Footer/>
                    {/* <!-- End of Footer --> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default JadwalPresensi;