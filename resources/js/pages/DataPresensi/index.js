import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Navigation
import Sidebar from '../../components/Navigation/Sidebar';
import Topbar from '../../components/Navigation/Topbar';
import Footer from '../../components/Navigation/Footer';
import Pagination from "react-js-pagination";

import PageHeading from '../../components/PageHeading';
import api from '../../service/api';
// import { getRiwayatPresensi } from '../../service/presensi';

class DataPresensi extends Component {
    constructor(){
        super();
        this.state = {
            role: '',
            datas: [],
            dataNotHadir: [],

            // pagination
            currentData: [],
            activePage: 1,
            itemPerPage : 10,
            
            currentDataNotHadir: [],
            activePageNotHadir: 1,
            itemPerPageNotHadir : 10,

            // search
            searchData: [],
            searchValue :"",

            // filter
            filterStatus:'',
            filterKesehatan:'',
            filterData: [],
        };
        this.handleSearchChange = this.handleSearchChange.bind(this);
        this.handleFilterChange = this.handleFilterChange.bind(this);
        this.handleFilterChangeNotHadir = this.handleFilterChangeNotHadir.bind(this);
    }

    componentDidMount(){
        this.setState({
            role: localStorage.getItem("user_role")
        });
        api().get('api/presensi/kehadiranToday').then(response =>{
            if(response.data.status == 'success'){
                this.setState({
                    datas: response.data.data
                })
            }
            else{
                alert(response.data.message);
            }
        })
        api().get('api/presensi/notKehadiranToday').then(response => {
            if (response.data.status == 'success') {
                this.setState({
                    dataNotHadir: response.data.data
                })
            }
            else {
                alert(response.data.message);
            }
        })
    }

    handleSearchChange(e){
        console.log(`search key is ${e.target.value}`);
        this.setState({searchValue: e.target.value});

        const searchData =
            this.state.filterData.filter(mhs => {
                    return mhs.nama_mhs.toLowerCase().includes(this.state.searchValue.toLowerCase())
            }
        )
        this.setState( {searchData} );
        this.setState({activePage: 1});
    }

    handleFilterChange(e){
        if(e.target.name==="status_presensi"){
            this.setState({filterStatus: e.target.value});
        }else{
        if(e.target.name==="kondisi_kesehatan"){
            this.setState({filterKesehatan: e.target.value});
        }}

        if(e.target.name==="itemPerPage"){
            this.setState({itemPerPage: parseInt(e.target.value)});
        }

        let filterData =
            this.state.datas.filter(mhs => {
                return mhs.status_presensi.toString().includes( this.state.filterStatus)
            }
        )

        filterData =
            filterData.filter(mhs => {
                return mhs.kondisi_kesehatan.substr(0, 5).includes(this.state.filterKesehatan)
            }
        )

        this.setState({ filterData });
        this.setState({ activePage: 1});
    }

    handleFilterChangeNotHadir(e){

        if(e.target.name==="itemPerPage"){
            this.setState({itemPerPageNotHadir: parseInt(e.target.value)});
        }

        this.setState({ activePageNotHadir: 1});
    }

    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage: pageNumber});

        const data = this.state.searchData;
        const offset = (this.state.activePage - 1) * this.state.itemPerPage;
        const currentData = data.slice(offset, offset + this.state.itemPerPage);

        this.setState({ currentData });
    }

    handlePageChangeNotHadir(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePageNotHadir: pageNumber});

        const dataNotHadir = this.state.dataNotHadir;
        const offsetNotHadir = (this.state.activePageNotHadir - 1) * this.state.itemPerPageNotHadir;
        const currentDataNotHadir = dataNotHadir.slice(offsetNotHadir, offsetNotHadir + this.state.itemPerPageNotHadir);

        this.setState({ currentDataNotHadir });
    }

    render() {
        //filter
        let filterData =
            this.state.datas.filter(mhs => {
                return mhs.status_presensi.toString().includes( this.state.filterStatus)
            }
        )
        filterData =
            filterData.filter(mhs => {
                return mhs.kondisi_kesehatan.substr(0, 5).includes(this.state.filterKesehatan)
            }
        )

        //search
        const searchData =
            filterData.filter(mhs => {
                    return mhs.nama_mhs.toLowerCase().includes(this.state.searchValue.toLowerCase())
            }
        )

        // pagination
        const offset = (this.state.activePage - 1) * this.state.itemPerPage;
        const currentData = searchData.slice(offset, offset + this.state.itemPerPage);

        const dataNotHadir = this.state.dataNotHadir;
        const offsetNotHadir = (this.state.activePageNotHadir - 1) * this.state.itemPerPageNotHadir;
        const currentDataNotHadir = dataNotHadir.slice(offsetNotHadir, offsetNotHadir + this.state.itemPerPageNotHadir);

        let TableStatus;
        if (this.state.datas.length == 0) {
            TableStatus = <h6 className="text-center">Tidak ada mahasiswa yang sudah presensi</h6>;
          } else {
            TableStatus = <div className="form-inline"><label for="itemPerPage">Jumlah per halaman</label>
                            <div className="mb-2 mx-2">
                                <select name="itemPerPage" id="itemPerPage" className="form-control" onChange={this.handleFilterChange}>
                                    <option value={10}>10</option>
                                    <option value={25}>25</option>
                                    <option value={50}>50</option>
                                    <option value={100}>100</option>
                                </select>
                            </div></div>;
        }

        let TableStatusNotHadir;
        if (dataNotHadir.length == 0) {
            TableStatusNotHadir = <h6 className="text-center">Tidak ada mahasiswa yang belum presensi</h6>;
          } else {
            TableStatusNotHadir = <div className="form-inline"><label for="itemPerPage">Jumlah per halaman</label>
                            <div className="mb-2 mx-2">
                                <select name="itemPerPage" id="itemPerPage" className="form-control" onChange={this.handleFilterChangeNotHadir}>
                                    <option value={10}>10</option>
                                    <option value={25}>25</option>
                                    <option value={50}>50</option>
                                    <option value={100}>100</option>
                                </select>
                            </div></div>;
        }

        return (
            <div>
                <div id="wrapper">
                {/* <!-- Sidebar --> */}
                <Sidebar role= {this.state.role} />
                {/* <!-- End of Sidebar --> */}
                    <div id="content-wrapper" className="d-flex flex-column">
                        <div id="content">
                        {/* <!-- Topbar --> */}
                        <Topbar />
                        {/* <!-- End of Topbar --> */}
                        <div className="container-fluid">
                            <PageHeading title="Presensi Harian Asrama" />

                            {/* Path */}
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to="#">Home</Link></li>
                                    <li className="breadcrumb-item active" aria-current="page">Data Presensi</li>
                                </ol>
                            </nav>

                            <div className="col-lg-12 col-md-12">
                                <div className="card my-5">
                                    <div className="card-header">
                                        <h6 className="text-primary">Mahasiswa yang sudah melakukan presensi</h6>
                                    </div>
                                    <div className="card-body">

                                        {/* Search Bar */}
                                        <div className="input-group mb-2 border rounded-pill p-1 col-lg-4 col-md-8 col-sm-12">
                                            <input type="text" name="searchValue" placeholder="Cari mahasiswa.." className="form-control bg-none border-0" onChange={this.handleSearchChange}/>
                                            <div className="input-group-append border-0">
                                                <button type="submit" className="btn btn-link text-primary"><i className="fa fa-search"></i></button>
                                            </div>
                                        </div>

                                        <div className="row">
                                            {/* Filter status */}
                                            <label for="status_presensi" className="col-form-label">Status presensi</label>
                                            <div className="mb-2 mx-2">
                                                <select name="status_presensi" class="form-control" onChange={this.handleFilterChange}>
                                                    <option value="">...</option>
                                                    <option value="0">Alpa</option>
                                                    <option value="1">Hadir</option>
                                                    <option value="2">Izin</option>
                                                </select>
                                            </div>

                                            {/* Filter kesehatan */}
                                            <label for="kondisi_kesehatan" className="col-form-label">Kondisi kesehatan</label>
                                            <div className="mb-2 mx-2">
                                                <select name="kondisi_kesehatan" class="form-control" onChange={this.handleFilterChange}>
                                                    <option value="">...</option>
                                                    <option value="Sehat">Sehat</option>
                                                    <option value="Sakit">Sakit</option>
                                                </select>
                                            </div>
                                        </div>

                                        {/* Tabel Presensi */}
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Nama</th>
                                                        <th scope="col">NIM</th>
                                                        <th scope="col">Kamar</th>
                                                        <th scope="col">Jenis</th>
                                                        <th scope="col">Presensi</th>
                                                        <th scope="col">Waktu</th>
                                                        <th scope="col">Status</th>
                                                        <th scope="col">Kondisi</th>
                                                        <th scope="col">Suhu badan</th>
                                                        <th scope="col"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {currentData.map(presensi => {
                                                        const {
                                                            id,
                                                            status_presensi,
                                                            kondisi_kesehatan,
                                                            suhu_badan,
                                                            created_at,
                                                            nama_mhs,
                                                            nim,
                                                            no_kamar,
                                                            nama_gedung,
                                                            keterangan_asal
                                                        } = presensi;
                                                        let status, color;
                                                        if(presensi.status_presensi == 0){
                                                            status = "Alpa"
                                                            color = "badge badge-pill badge-danger"
                                                        }
                                                        else if(presensi.status_presensi == 1){
                                                            status = "Hadir"
                                                            color = "badge badge-pill badge-success"
                                                        }
                                                        else{
                                                            status = "Izin"
                                                            color = "badge badge-pill badge-warning"
                                                        }
                                                        return (
                                                            <tr>
                                                                <td className="text-uppercase">{presensi.nama_mhs}</td>
                                                                <td>{presensi.nim}</td>
                                                                <td>{presensi.nama_gedung}-{presensi.no_kamar}</td>
                                                                <td>{presensi.keterangan_asal}</td>
                                                                <td>{presensi.nama_jadwal}</td>
                                                                <td>{new Date(presensi.created_at).toLocaleTimeString()}</td>
                                                                <td>
                                                                    <span className={color}>{status}
                                                                    </span>
                                                                </td>
                                                                <td>{presensi.kondisi_kesehatan.substr(0, 5)}</td>
                                                                <td>{presensi.suhu_badan} &deg;C</td>
                                                                <td>
                                                                    <Link to={"/detail-presensi/" + presensi.id_mhs} className="text-primary mx-1">
                                                                        <i className="fas fa-info-circle"></i>
                                                                    </Link>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>

                                        {TableStatus}
                                        
                                        {/* pagination */}
                                        <div className="d-flex justify-content-end">
                                            <Pagination
                                                itemClass="page-item"
                                                linkClass="page-link"
                                                activePage={this.state.activePage}
                                                itemsCountPerPage={this.state.itemPerPage}
                                                totalItemsCount={searchData.length}
                                                pageRangeDisplayed={3}
                                                onChange={this.handlePageChange.bind(this)}
                                            />
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-12 col-md-12">
                                <div className="card my-5">
                                    <div className="card-header">
                                        <h6 className="text-primary">Mahasiswa yang belum melakukan presensi</h6>
                                    </div>
                                    <div className="card-body">

                                        {/* Tabel Presensi */}
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Nama</th>
                                                        <th scope="col">NIM</th>
                                                        <th scope="col">Kamar</th>
                                                        <th scope="col">Jenis</th>
                                                        <th scope="col">Presensi</th>
                                                        <th scope="col">Jadwal</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {currentDataNotHadir.map(presensi => {
                                                        const {
                                                            id,
                                                            status_presensi,
                                                            kondisi_kesehatan,
                                                            suhu_badan,
                                                            created_at,
                                                            nama_mhs,
                                                            nim,
                                                            no_kamar,
                                                            nama_gedung,
                                                            keterangan_asal
                                                        } = presensi;
                                                        let status, color;
                                                        if(presensi.status_presensi == 0){
                                                            status = "Alpa"
                                                            color = "badge badge-pill badge-danger"
                                                        }
                                                        else if(presensi.status_presensi == 1){
                                                            status = "Hadir"
                                                            color = "badge badge-pill badge-success"
                                                        }
                                                        else{
                                                            status = "Izin"
                                                            color = "badge badge-pill badge-warning"
                                                        }
                                                        return (
                                                            <tr>
                                                                <td className="text-uppercase">{presensi.nama_mhs}</td>
                                                                <td>{presensi.nim}</td>
                                                                <td>{presensi.nama_gedung}-{presensi.no_kamar}</td>
                                                                <td>{presensi.keterangan_asal}</td>
                                                                <td>{presensi.nama_jadwal}</td>
                                                                <td>{presensi.jam_mulai} - {presensi.jam_selesai}</td>
                                                            </tr>
                                                        )
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>

                                        {TableStatusNotHadir}
                                        
                                        {/* pagination */}
                                        <div className="d-flex justify-content-end">
                                            <Pagination
                                                itemClass="page-item"
                                                linkClass="page-link"
                                                activePage={this.state.activePageNotHadir}
                                                itemsCountPerPage={this.state.itemPerPageNotHadir}
                                                totalItemsCount={dataNotHadir.length}
                                                pageRangeDisplayed={3}
                                                onChange={this.handlePageChangeNotHadir.bind(this)}
                                            />
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    {/* <!-- Footer --> */}
                    <Footer/>
                    {/* <!-- End of Footer --> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default DataPresensi;
