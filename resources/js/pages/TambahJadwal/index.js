import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Navigation
import Sidebar from '../../components/Navigation/Sidebar';
import Topbar from '../../components/Navigation/Topbar';
import Footer from '../../components/Navigation/Footer';

import PageHeading from '../../components/PageHeading';

import api from '../../service/api';

function loadingAnimation() {
    return new Promise(function(resolve) {
      setTimeout(() => resolve([1, 2, 3]), 1000);
    });
}

class TambahJadwal extends Component {
    constructor(){
        super();
        this.state = {
            role: "",
            errList: [],

            //loading
            isLoading:false,
            list: [],

        };
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    handleFieldChange(e){
        let name = e.target.name;
        let value = e.target.value;
        let data = {};
        data[name] = value;
        this.setState(data);
    }

    async handleSubmit(e){
        this.setState({ isLoading: true });

        e.preventDefault();
        await api().post('api/jadwal/store', ({
            nama_jadwal: this.state.nama_jadwal,
            jam_mulai: this.state.jam_mulai,
            jam_selesai: this.state.jam_selesai,
        })).then(response => {
            if(response.data.status === 'success'){
                window.location.assign('/#/jadwal-presensi')
            }
            else{
                this.setState({
                    errList: response.data.message
                })
            }
        })

        // Set status animasi loading
        loadingAnimation().then(list => {
            this.setState({
              isLoading: false,
              list,
            });
        });
    }

    componentDidMount(){
        this.setState({
            role: localStorage.getItem("user_role")
        });
    }

    render() {
        return (
            <div>
                <div id="wrapper">
                {/* <!-- Sidebar --> */}
                <Sidebar role= {this.state.role} />
                {/* <!-- End of Sidebar --> */}
                    <div id="content-wrapper" className="d-flex flex-column">
                        <div id="content">
                        {/* <!-- Topbar --> */}
                        <Topbar />
                        {/* <!-- End of Topbar --> */}
                        <div className="container-fluid">
                            <PageHeading title="Tambah Jadwal Presensi" />

                            {/* Path */}
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to="#">Home</Link></li>
                                    <li className="breadcrumb-item"><Link to="/jadwal-presensi">Jadwal Presensi</Link></li>
                                    <li className="breadcrumb-item active" aria-current="page">Tambah Jadwal</li>
                                </ol>
                            </nav>

                            <div className="col-lg-12 col-md-12">
                                <div className="card my-5">
                                    <div className="card-body">

                                        {/* Tambah jadwal*/}
                                        <form>
                                            <div className="form-group row">
                                                <label for="nama" className="col-md-3 col-form-label text-md-right">Nama jadwal</label>
                                                <div className="col-md-8">
                                                    <input 
                                                        type="text" 
                                                        name="nama_jadwal"
                                                        onChange={this.handleFieldChange}
                                                        className="form-control"
                                                        placeholder="Masukan nama jadwal"
                                                    />
                                                    <span className="text-danger">{this.state.errList.nama_jadwal}</span>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label for="alamat" className="col-md-3 col-form-label text-md-right">Jam mulai</label>
                                                <div className="col-md-8">
                                                    <input 
                                                        type="time"
                                                        name="jam_mulai"
                                                        onChange={this.handleFieldChange} 
                                                        className="form-control"
                                                    />
                                                    <span className="text-danger">{this.state.errList.jam_mulai}</span>
                                                </div>
                                            </div><div className="form-group row">
                                                <label for="alamat" className="col-md-3 col-form-label text-md-right">Jam selesai</label>
                                                <div className="col-md-8">
                                                    <input 
                                                        type="time"
                                                        name="jam_selesai"
                                                        onChange={this.handleFieldChange} 
                                                        className="form-control"
                                                    />
                                                    <span className="text-danger">{this.state.errList.jam_selesai}</span>
                                                </div>
                                            </div>
                                            
                                            <div className="form-group row">
                                                <div className="col-md-8 offset-md-3 mb-2">
                                                    <button type="submit" className="btn btn-success" onClick={this.handleSubmit} disabled={this.state.isLoading}>
                                                        {this.state.isLoading ? <i className="fas fa-spinner fa-pulse"></i> : <i className="fas fa-sign-in-alt"></i>} Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- Footer --> */}
                    <Footer/>
                    {/* <!-- End of Footer --> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default TambahJadwal;