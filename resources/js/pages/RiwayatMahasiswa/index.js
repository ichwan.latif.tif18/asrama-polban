import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Navigation
import Sidebar from '../../components/Navigation/Sidebar';
import Topbar from '../../components/Navigation/Topbar';
import Footer from '../../components/Navigation/Footer';
import Pagination from "react-js-pagination";

import PageHeading from '../../components/PageHeading';
import api from '../../service/api';

class RiwayatMahasiswa extends Component {
    constructor(){
        super();
        this.state = {
            role: '',
            datapresensi: [],
            datasIzin: [],
            datasResign: [],

            // pagination
            currentDataPresensi: [],
            activePagePresensi: 1,
            itemPerPagePresensi : 10,
            
            currentDataPerizinan: [],
            activePagePerizinan: 1,
            itemPerPagePerizinan : 10,

            // filter
            filterStatus:'',
            filterKesehatan:'',
            filterDataPresensi: [],
        };
        this.handleFilterChangePresensi = this.handleFilterChangePresensi.bind(this);
        this.handleFilterChangePerizinan = this.handleFilterChangePerizinan.bind(this);
    }

    componentDidMount(){
        this.setState({
            role: localStorage.getItem("user_role")
        });
        
        const {id} = this.props.match.params
        api().get('api/presensi/user/' + id).then(response =>{
            if(response.data.status === 'success'){
                this.setState({
                    datapresensi: response.data.data
                })
            }
            else{
                alert(response.data.message);
            }
        })

        api().get('api/perizinan/riwayatperizinan/' + id).then(perizinan =>{
            if(perizinan.data.status === 'success'){
                this.setState({
                    datasIzin: perizinan.data.data
                })
            }
            else{
                alert(perizinan.data.message);
            }
        })

        api().get('api/resign/riwayatresign/' + id).then(resign => {
            if(resign.data.status === 'success'){
                this.setState({
                    datasResign: resign.data.data
                })
            }
            else{
                alert(resign.data.message);
            }
        })
    }

    handleFilterChangePresensi(e){
        if(e.target.name==="status_presensi"){
            this.setState({filterStatus: e.target.value});
        }else{
        if(e.target.name==="kondisi_kesehatan"){
            this.setState({filterKesehatan: e.target.value});
        }}

        if(e.target.name==="itemPerPagePresensi"){
            this.setState({itemPerPagePresensi: parseInt(e.target.value)});
        }

        let filterDataPresensi =
            this.state.datapresensi.filter(mhs => {
                return mhs.status_presensi.toString().includes( this.state.filterStatus)
            }
        )

        filterDataPresensi =
            filterDataPresensi.filter(mhs => {
                return mhs.kondisi_kesehatan.substr(0, 5).includes(this.state.filterKesehatan)
            }
        )

        this.setState({ filterDataPresensi });
        this.setState({ activePagePresensi: 1});
    }

    handleFilterChangePerizinan(e){
        if(e.target.name==="itemPerPagePerizinan"){
            this.setState({itemPerPagePerizinan: parseInt(e.target.value)});
        }
        
        this.setState({ activePagePerizinan: 1});
    }

    handlePageChangePresensi(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePagePresensi: pageNumber});

        const data = this.state.datapresensi;
        const offsetPresensi = (this.state.activePagePresensi - 1) * this.state.itemPerPagePresensi;
        const currentDataPresensi = data.slice(offsetPresensi, offsetPresensi + this.state.itemPerPagePresensi);

        this.setState({ currentDataPresensi });
    }

    handlePageChangePerizinan(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePagePerizinan: pageNumber});

        const data = [...dataIzin, ...dataResign];
        const offsetPerizinan = (this.state.activePagePerizinan - 1) * this.state.itemPerPagePerizinan;
        const currentDataPerizinan = data.slice(offsetPerizinan, offsetPerizinan + this.state.itemPerPagePerizinan);

        this.setState({ currentDataPerizinan });
    }

    render() {
        //filter
        let filterDataPresensi =
            this.state.datapresensi.filter(mhs => {
                return mhs.status_presensi.toString().includes( this.state.filterStatus)
            }
        )
        filterDataPresensi =
            filterDataPresensi.filter(mhs => {
                return mhs.kondisi_kesehatan.substr(0, 5).includes(this.state.filterKesehatan)
            }
        )

        // pagination
        const offsetPresensi = (this.state.activePagePresensi - 1) * this.state.itemPerPagePresensi;
        const currentDataPresensi = filterDataPresensi.slice(offsetPresensi, offsetPresensi + this.state.itemPerPagePresensi);
        
        let TableStatusPresensi;
        if (this.state.datapresensi.length == 0) {
            TableStatusPresensi = <h6 className="text-center">Tidak ada presensi</h6>;
          } else {
            TableStatusPresensi = <div className="form-inline"><label for="itemPerPagePresensi">Jumlah per halaman</label>
                            <div className="mb-2 mx-2">
                                <select name="itemPerPagePresensi" id="itemPerPagePresensi" className="form-control" onChange={this.handleFilterChangePresensi}>
                                    <option value={10}>10</option>
                                    <option value={25}>25</option>
                                    <option value={50}>50</option>
                                    <option value={100}>100</option>
                                </select>
                            </div></div>;
        }

        // pagination
        
        const dataPerizinan = [...this.state.datasResign, ...this.state.datasIzin];
        const offsetPerizinan = (this.state.activePagePerizinan - 1) * this.state.itemPerPagePerizinan;
        const currentDataPerizinan = dataPerizinan.slice(offsetPerizinan, offsetPerizinan + this.state.itemPerPagePerizinan);
        console.log(currentDataPerizinan)

        let TableStatusPerizinan;
        if (this.state.datasIzin.length == 0 && this.state.datasResign.length == 0) {
            TableStatusPerizinan = <h6 className="text-center">Tidak ada perizinan</h6>;
          } else {
            TableStatusPerizinan = <div className="form-inline"><label for="itemPerPagePerizinan">Jumlah per halaman</label>
                            <div className="mb-2 mx-2">
                                <select name="itemPerPagePerizinan" id="itemPerPagePerizinan" className="form-control" onChange={this.handleFilterChangePerizinan}>
                                    <option value={10}>10</option>
                                    <option value={25}>25</option>
                                    <option value={50}>50</option>
                                    <option value={100}>100</option>
                                </select>
                            </div></div>;
        }

        return (
            <div>
                <div id="wrapper">
                {/* <!-- Sidebar --> */}
                <Sidebar role= {this.state.role} />
                {/* <!-- End of Sidebar --> */}
                    <div id="content-wrapper" className="d-flex flex-column">
                        <div id="content">
                        {/* <!-- Topbar --> */}
                        <Topbar />
                        {/* <!-- End of Topbar --> */}
                        <div className="container-fluid">
                            <PageHeading title="Riwayat Mahasiswa" />
                            
                            {/* Path */}
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to="#">Home</Link></li>
                                    <li className="breadcrumb-item active" aria-current="page">Riwayat Mahasiswa</li>
                                </ol>
                            </nav>

                            <div className="col-lg-12 col-md-12">
                                <div className="card my-5">
                                    <div className="card-header">
                                        <h6 className="text-primary">Riwayat Presensi</h6>
                                    </div>

                                    <div className="card-body">

                                        <div className="row">
                                            {/* Filter status */}
                                            <label for="status_presensi" className="col-form-label">Status presensi</label>
                                            <div className="mb-2 mx-2">
                                                <select name="status_presensi" class="form-control" onChange={this.handleFilterChangePresensi}>
                                                    <option value="" selected >...</option>
                                                    <option value="0">Alpa</option>
                                                    <option value="1">Hadir</option>
                                                    <option value="2">Izin</option>
                                                </select>
                                            </div>

                                            {/* Filter kesehatan */}
                                            <label for="kondisi_kesehatan" className="col-form-label">Kondisi kesehatan</label>
                                            <div className="mb-2 mx-2">
                                                <select name="kondisi_kesehatan" class="form-control" onChange={this.handleFilterChangePresensi}>
                                                    <option value="" selected >...</option>
                                                    <option value="Sehat">Sehat</option>
                                                    <option value="Sakit">Sakit</option>
                                                </select>
                                            </div>
                                        </div>

                                        {/* Tabel Presensi */}
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                                <thead>
                                                    <tr>
                                                    <th scope="col">Tanggal</th>
                                                    <th scope="col">Waktu presensi</th>
                                                    <th scope="col">Koordinat GPS</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Kondisi</th>
                                                    <th scope="col">Suhu badan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {currentDataPresensi.map(presensi => {
                                                        const {
                                                            id,
                                                            status_presensi,
                                                            latitude,
                                                            longitude,
                                                            kondisi_kesehatan,
                                                            suhu_badan,
                                                            created_at,
                                                        } = presensi;
                                                        let status, color;
                                                        if(presensi.status_presensi === 0){
                                                            status = "Alpa"
                                                            color = "badge badge-pill badge-danger"
                                                        }
                                                        else if(presensi.status_presensi === 1){
                                                            status = "Hadir"
                                                            color = "badge badge-pill badge-success"
                                                        }
                                                        else{
                                                            status = "Izin"
                                                            color = "badge badge-pill badge-warning"
                                                        }
                                                        return (
                                                            <tr>
                                                                <td>{new Date(presensi.created_at).toLocaleDateString()}</td>
                                                                <td>{new Date(presensi.created_at).toLocaleTimeString()}</td>
                                                                <td>{presensi.latitude + ', ' + presensi.longitude}</td>
                                                                <td>
                                                                    <span className={color}>{status}
                                                                    </span>
                                                                </td>
                                                                <td>{presensi.kondisi_kesehatan.substr(0, 5)}</td>
                                                                <td>{presensi.suhu_badan} &deg;C</td>
                                                            </tr>
                                                        )
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>

                                        {TableStatusPresensi}
                                        
                                        {/* pagination */}
                                        <div className="d-flex justify-content-end">
                                            <Pagination
                                                itemClass="page-item"
                                                linkClass="page-link"
                                                activePage={this.state.activePagePresensi}
                                                itemsCountPerPage={this.state.itemPerPagePresensi}
                                                totalItemsCount={filterDataPresensi.length}
                                                pageRangeDisplayed={3}
                                                onChange={this.handlePageChangePresensi.bind(this)}
                                            />
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-12 col-md-12">
                                <div className="card my-5">
                                    <div className="card-header">
                                        <h6 className="text-primary">Riwayat Perizinan</h6>
                                    </div>

                                    <div className="card-body">

                                        {/* Tabel Perizinan */}
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                                <thead>
                                                    <tr>
                                                    <th scope="col">Mulai</th>
                                                    <th scope="col">Berakhir</th>
                                                    <th scope="col">Jenis izin</th>
                                                    <th scope="col">Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    {currentDataPerizinan.map(perizinan => {
                                                        const {
                                                            id,
                                                            tanggal_resign,
                                                            keterangan_resign,
                                                            status_resign,

                                                            id_perizinan,
                                                            tanggal_pergi,
                                                            tanggal_pulang,
                                                            status_izin,
                                                        } = perizinan;
                                                        let statusResign;
                                                        switch (perizinan.status_resign) {
                                                            case 0:
                                                                statusResign = "Mengajukan"
                                                                break;
                                                            case 1:
                                                                statusResign = "Disetujui Pengelola"
                                                                break;
                                                            case 2:
                                                                statusResign = "Ditolak Pengelola"
                                                                break;
                                                            case 3:
                                                                statusResign = "Disetujui Wadir 3"
                                                                break;
                                                            case 4:
                                                                statusResign = "Ditolak Wadir 3"
                                                                break;
                                                            default:
                                                                statusResign = ""
                                                                break;
                                                        }
                                                        let status;
                                                        switch (perizinan.status_izin) {
                                                            case 0:
                                                                status = "Mengajukan"
                                                                break;
                                                            case 1:
                                                                status = "Disetujui Pengelola"
                                                                break;
                                                            case 2:
                                                                status = "Ditolak Pengelola"
                                                                break;
                                                            case 3:
                                                                status = "Disetujui Wadir 3"
                                                                break;
                                                            case 4:
                                                                status = "Ditolak Wadir 3"
                                                                break;
                                                            case 5:
                                                                status = "Mengajukan Kembali"
                                                                break;
                                                            case 6:
                                                                status = "Disetujui Kembali oleh Pengelola"
                                                                break;
                                                            case 7:
                                                                status = "Ditolak Kembali oleh Pengelola"
                                                                break;
                                                            case 8:
                                                                status = "Disetujui Kembali oleh Wadir 3"
                                                                break;
                                                            case 9:
                                                                status = "Ditolak Kembali oleh Wadir 3"
                                                                break;
                                                            case 10:
                                                                status = "Terkonfirmasi di asrama"
                                                                break;
                                                            default:
                                                                status = ""
                                                                break;
                                                        }
                                                        let jenis;
                                                        if(perizinan.tanggal_resign){
                                                            jenis = "Resign"
                                                        }else if(perizinan.tanggal_pulang){
                                                            jenis = "Pulang"
                                                        }

                                                        return (
                                                            <tr>
                                                                <td>{perizinan.tanggal_resign}{perizinan.tanggal_pergi}</td>
                                                                <td>{perizinan.tanggal_pulang}</td>
                                                                <td>{jenis}</td>
                                                                <td>{statusResign}{status}</td>
                                                            </tr>
                                                        )
                                                    })}

                                                </tbody>
                                            </table>
                                        </div>

                                        {TableStatusPerizinan}
                                        
                                        {/* pagination */}
                                        <div className="d-flex justify-content-end">
                                        <Pagination
                                            itemClass="page-item"
                                            linkClass="page-link"
                                            activePage={this.state.activePagePerizinan}
                                            itemsCountPerPage={this.state.itemPerPagePerizinan}
                                            totalItemsCount={dataPerizinan.length}
                                            pageRangeDisplayed={3}
                                            onChange={this.handlePageChangePerizinan.bind(this)}
                                        />
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    {/* <!-- Footer --> */}
                    <Footer/>
                    {/* <!-- End of Footer --> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default RiwayatMahasiswa;