import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Navigation
import Sidebar from '../../components/Navigation/Sidebar';
import Topbar from '../../components/Navigation/Topbar';
import Footer from '../../components/Navigation/Footer';
import Pagination from "react-js-pagination";

import PageHeading from '../../components/PageHeading';
import api from '../../service/api';
// import { getRiwayatPresensi } from '../../service/presensi';

class RiwayatPresensi extends Component {
    constructor(){
        super();
        this.state = {
            role: '',
            datas: [],

            // pagination
            currentData: [],
            activePage: 1,
            itemPerPage : 10,

            // filter
            filterStatus:'',
            filterKesehatan:'',
            filterData: [],
        };
        this.handleFilterChange = this.handleFilterChange.bind(this);
    }

    componentDidMount(){
        this.setState({
            role: localStorage.getItem("user_role")
        });
        api().get('api/presensi/user/' + localStorage.getItem('user_id')).then(response =>{
            if(response.data.status == 'success'){
                this.setState({
                    datas: response.data.data
                })
                console.log(this.state.datas)
            }
            else{
                alert(response.data.message);
            }
        })
    }

    handleFilterChange(e){
        if(e.target.name==="status_presensi"){
            this.setState({filterStatus: e.target.value});
        }else{
        if(e.target.name==="kondisi_kesehatan"){
            this.setState({filterKesehatan: e.target.value});
        }}

        if(e.target.name==="itemPerPage"){
            this.setState({itemPerPage: parseInt(e.target.value)});
        }

        let filterData =
            this.state.datas.filter(mhs => {
                return mhs.status_presensi.toString().includes( this.state.filterStatus)
            }
        )

        filterData =
            filterData.filter(mhs => {
                return mhs.kondisi_kesehatan.substr(0, 5).includes(this.state.filterKesehatan)
            }
        )

        this.setState({ filterData });
        this.setState({ activePage: 1});
    }

    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage: pageNumber});

        const data = this.state.datas;
        const offset = (this.state.activePage - 1) * this.state.itemPerPage;
        const currentData = data.slice(offset, offset + this.state.itemPerPage);

        this.setState({ currentData });
    }

    render() {
        //filter
        let filterData =
            this.state.datas.filter(mhs => {
                return mhs.status_presensi.toString().includes( this.state.filterStatus)
            }
        )
        filterData =
            filterData.filter(mhs => {
                return mhs.kondisi_kesehatan.substr(0, 5).includes(this.state.filterKesehatan)
            }
        )

        // pagination
        const offset = (this.state.activePage - 1) * this.state.itemPerPage;
        const currentData = filterData.slice(offset, offset + this.state.itemPerPage);

        let TableStatus;
        if (this.state.datas.length == 0) {
            TableStatus = <h6 className="text-center">Tidak ada presensi</h6>;
          } else {
            TableStatus = <div className="form-inline"><label for="itemPerPage">Jumlah per halaman</label>
                            <div className="mb-2 mx-2">
                                <select name="itemPerPage" id="itemPerPage" className="form-control" onChange={this.handleFilterChange}>
                                    <option value={10}>10</option>
                                    <option value={25}>25</option>
                                    <option value={50}>50</option>
                                    <option value={100}>100</option>
                                </select>
                            </div></div>;
        }

        return (
            <div>
                <div id="wrapper">
                {/* <!-- Sidebar --> */}
                <Sidebar role= {this.state.role} />
                {/* <!-- End of Sidebar --> */}
                    <div id="content-wrapper" className="d-flex flex-column">
                        <div id="content">
                        {/* <!-- Topbar --> */}
                        <Topbar />
                        {/* <!-- End of Topbar --> */}
                        <div className="container-fluid">
                            <PageHeading title="Riwayat Presensi" />
                            
                            {/* Path */}
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to="#">Home</Link></li>
                                    <li className="breadcrumb-item active" aria-current="page">Riwayat Presensi</li>
                                </ol>
                            </nav>

                            <div className="col-lg-12 col-md-12">
                                <div className="card my-5">
                                    <div className="card-header">
                                        <h6 className="text-primary">Riwayat Presensi</h6>
                                    </div>

                                    <div className="card-body">

                                        <div className="row">
                                            {/* Filter status */}
                                            <label for="status_presensi" className="col-form-label">Status presensi</label>
                                            <div className="mb-2 mx-2">
                                                <select name="status_presensi" class="form-control" onChange={this.handleFilterChange}>
                                                    <option value="" selected >...</option>
                                                    <option value="0">Alpa</option>
                                                    <option value="1">Hadir</option>
                                                    <option value="2">Izin</option>
                                                </select>
                                            </div>

                                            {/* Filter kesehatan */}
                                            <label for="kondisi_kesehatan" className="col-form-label">Kondisi kesehatan</label>
                                            <div className="mb-2 mx-2">
                                                <select name="kondisi_kesehatan" class="form-control" onChange={this.handleFilterChange}>
                                                    <option value="" selected >...</option>
                                                    <option value="Sehat">Sehat</option>
                                                    <option value="Sakit">Sakit</option>
                                                </select>
                                            </div>
                                        </div>

                                        {/* Tabel Presensi */}
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                                <thead>
                                                    <tr>
                                                    <th scope="col">Nama Presensi</th>
                                                    <th scope="col">Tanggal</th>
                                                    <th scope="col">Waktu presensi</th>
                                                    <th scope="col">Koordinat GPS</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Kondisi</th>
                                                    <th scope="col">Suhu badan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {currentData.map(presensi => {
                                                        const {
                                                            id,
                                                            status_presensi,
                                                            latitude,
                                                            longitude,
                                                            kondisi_kesehatan,
                                                            suhu_badan,
                                                            created_at,
                                                        } = presensi;
                                                        let status, color;
                                                        if(presensi.status_presensi == 0){
                                                            status = "Alpa"
                                                            color = "badge badge-pill badge-danger"
                                                        }
                                                        else if(presensi.status_presensi == 1){
                                                            status = "Hadir"
                                                            color = "badge badge-pill badge-success"
                                                        }
                                                        else{
                                                            status = "Izin"
                                                            color = "badge badge-pill badge-warning"
                                                        }
                                                        return (
                                                            <tr>
                                                                <td>{presensi.nama_jadwal}</td>
                                                                <td>{new Date(presensi.created_at).toLocaleDateString()}</td>
                                                                <td>{new Date(presensi.created_at).toLocaleTimeString()}</td>
                                                                <td>{presensi.latitude + ', ' + presensi.longitude}</td>
                                                                <td>
                                                                    <span className={color}>{status}
                                                                    </span>
                                                                </td>
                                                                <td>{presensi.kondisi_kesehatan.substr(0, 5)}</td>
                                                                <td>{presensi.suhu_badan} &deg;C</td>
                                                            </tr>
                                                        )
                                                    })}
                                                </tbody>
                                            </table>
                                        </div>

                                        {TableStatus}
                                        
                                        {/* pagination */}
                                        <div className="d-flex justify-content-end">
                                            <Pagination
                                                itemClass="page-item"
                                                linkClass="page-link"
                                                activePage={this.state.activePage}
                                                itemsCountPerPage={this.state.itemPerPage}
                                                totalItemsCount={filterData.length}
                                                pageRangeDisplayed={3}
                                                onChange={this.handlePageChange.bind(this)}
                                            />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- Footer --> */}
                    <Footer/>
                    {/* <!-- End of Footer --> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default RiwayatPresensi;