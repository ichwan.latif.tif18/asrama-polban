import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Navigation
import Sidebar from '../../components/Navigation/Sidebar';
import Topbar from '../../components/Navigation/Topbar';
import Footer from '../../components/Navigation/Footer';

import PageHeading from '../../components/PageHeading';

import api from '../../service/api';

import MapView from '../../components/Map/MapView';

class DetailPresensi extends Component {
    constructor(){
        super();
        this.state = {
            role: "",
            lat: 0,
            long: 0,
        };
    }

    componentDidMount(){
        this.setState({
            role: localStorage.getItem("user_role")
        })

        const {id} = this.props.match.params
        api().get('api/presensi/getPresensiTodayById/' + id).then(responsePresensi =>{
            if(responsePresensi.data.status === 'success'){
                this.setState({
                    id_mhs:responsePresensi.data.data.id_mhs,
                    nama_mhs: responsePresensi.data.data.nama_mhs,
                    nim: responsePresensi.data.data.nim,
                    no_hp_mhs: responsePresensi.data.data.no_hp_mhs,
                    nama_ortu: responsePresensi.data.data.nama_ortu,
                    no_hp_ortu: responsePresensi.data.data.no_hp_ortu,
                    prodi: responsePresensi.data.data.nama_prodi,
                    jurusan: responsePresensi.data.data.nama_jurusan,
                    gedung: responsePresensi.data.data.nama_gedung,
                    kamar: responsePresensi.data.data.no_kamar,
                    lantai: responsePresensi.data.data.lantai,
                    lat: responsePresensi.data.data.latitude,
                    long: responsePresensi.data.data.longitude,
                    created_at: responsePresensi.data.data.created_at,
                    status_presensi: responsePresensi.data.data.status_presensi,
                    kondisi_kesehatan: responsePresensi.data.data.kondisi_kesehatan,
                    suhu_badan: responsePresensi.data.data.suhu_badan,
                    nama_jadwal: responsePresensi.data.data.nama_jadwal,
                    jam_mulai: responsePresensi.data.data.jam_mulai,
                    jam_selesai: responsePresensi.data.data.jam_selesai,

                });
                console.log(this.state.responsePresensi)
            }
            else{
                alert(responsePresensi.data.message);
            }
        })
        ;
    }

    render() {
        let status;
        switch (this.state.status_presensi){
            case 0:status="Alpa";break;
            case 1:status="Hadir";break;
            case 2:status="Izin";break;
            default:
        }

        return (
            <div>
                <div id="wrapper">
                {/* <!-- Sidebar --> */}
                <Sidebar role= {this.state.role} />
                {/* <!-- End of Sidebar --> */}
                    <div id="content-wrapper" className="d-flex flex-column">
                        <div id="content">
                        {/* <!-- Topbar --> */}
                        <Topbar />
                        {/* <!-- End of Topbar --> */}
                        <div className="container-fluid">
                            <PageHeading title="Detail Mahasiswa" />

                            {/* Path */}
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to="#">Home</Link></li>
                                    <li className="breadcrumb-item"><Link to="/data-presensi">Data Presensi</Link></li>
                                    <li className="breadcrumb-item active" aria-current="page">Detail Presensi</li>
                                </ol>
                            </nav>

                            <div className="col-lg-12 col-md-12">
                                <div className="card my-5">
                                    <div className="card-body">

                                        {/* Data presensi*/}
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Nama lengkap</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext text-uppercase">{this.state.nama_mhs}</div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Nomor handphone</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {this.state.no_hp_mhs}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Nama orangtua / wali</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext text-uppercase">{this.state.nama_ortu}</div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Nomor handphone orangtua / wali</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {this.state.no_hp_ortu}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Nomor Induk Mahasiswa</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {this.state.nim}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Jurusan / Program studi</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {this.state.jurusan +" / "+ this.state.prodi}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Kamar asrama</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {"Lt."+this.state.lantai+" / "+this.state.gedung+"-"+this.state.kamar}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Nama presensi</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {this.state.nama_jadwal}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Jadwal presensi</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {this.state.jam_mulai} s/d {this.state.jam_selesai}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Waktu presensi</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {this.state.created_at}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Lokasi presensi</label>
                                            <div className="col-md-8">
                                                <MapView
                                                    lat={this.state.lat}
                                                    lng={this.state.long}
                                                />
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Status presensi</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {status}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Kondisi kesehatan</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {this.state.kondisi_kesehatan}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <label className="col-md-3 col-form-label text-md-right">Suhu badan</label>
                                            <div className="col-md-8">
                                                <div className="form-control-plaintext">
                                                    {this.state.suhu_badan} &deg;Celcius
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- Footer --> */}
                    <Footer/>
                    {/* <!-- End of Footer --> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default DetailPresensi;