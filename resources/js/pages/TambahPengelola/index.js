import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Navigation
import Sidebar from '../../components/Navigation/Sidebar';
import Topbar from '../../components/Navigation/Topbar';
import Footer from '../../components/Navigation/Footer';

import PageHeading from '../../components/PageHeading';

import api from '../../service/api';

function loadingAnimation() {
    return new Promise(function(resolve) {
      setTimeout(() => resolve([1, 2, 3]), 1000);
    });
}

class TambahPengelola extends Component {
    constructor(){
        super();
        this.state = {
            role: "",
            errList: [],

            //loading
            isLoading:false,
            list: [],

        };
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    handleFieldChange(e){
        let name = e.target.name;
        let value = e.target.value;
        let data = {};
        data[name] = value;
        this.setState(data);
    }

    async handleSubmit(e){
        this.setState({ isLoading: true });

        e.preventDefault();
        await api().post('api/pengelola/store', ({
            email: this.state.email,
            password: this.state.password,
            nama_pengelola: this.state.nama_pengelola,
            alamat: this.state.alamat,
            no_hp_pengelola: this.state.no_hp_pengelola,
            jenis_kelamin: this.state.jenis_kelamin,
        })).then(response => {
            if(response.data.status === 'success'){
                window.location.assign('/#/data-pengelola')
            }
            else{
                this.setState({
                    errList: response.data.message
                })
            }
        })

        // Set status animasi loading
        loadingAnimation().then(list => {
            this.setState({
              isLoading: false,
              list,
            });
        });
    }

    componentDidMount(){
        this.setState({
            role: localStorage.getItem("user_role")
        });
    }

    render() {
        return (
            <div>
                <div id="wrapper">
                {/* <!-- Sidebar --> */}
                <Sidebar role= {this.state.role} />
                {/* <!-- End of Sidebar --> */}
                    <div id="content-wrapper" className="d-flex flex-column">
                        <div id="content">
                        {/* <!-- Topbar --> */}
                        <Topbar />
                        {/* <!-- End of Topbar --> */}
                        <div className="container-fluid">
                            <PageHeading title="Tambah Pengelola" />

                            {/* Path */}
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link to="#">Home</Link></li>
                                    <li className="breadcrumb-item"><Link to="/data-pengelola">Data Pengelola</Link></li>
                                    <li className="breadcrumb-item active" aria-current="page">Tambah Pengelola</li>
                                </ol>
                            </nav>

                            <div className="col-lg-12 col-md-12">
                                <div className="card my-5">
                                    <div className="card-body">

                                        {/* Tambah pengelola*/}
                                        <form>
                                            <div className="form-group row">
                                                <label for="email" className="col-md-3 col-form-label text-md-right">Email</label>
                                                <div className="col-md-8">
                                                    <input 
                                                        type="email"
                                                        name="email"
                                                        onChange={this.handleFieldChange} 
                                                        className="form-control"
                                                        placeholder="Masukan email polban"
                                                    />
                                                    <span className="text-danger">{this.state.errList.email}</span>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label for="password" className="col-md-3 col-form-label text-md-right">Password</label>
                                                <div className="col-md-8">
                                                    <input 
                                                        type="password"
                                                        name="password"
                                                        onChange={this.handleFieldChange} 
                                                        className="form-control"
                                                        placeholder="Masukan password"
                                                    />
                                                    <span className="text-danger">{this.state.errList.password}</span>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label for="nama" className="col-md-3 col-form-label text-md-right">Nama lengkap</label>
                                                <div className="col-md-8">
                                                    <input 
                                                        type="text" 
                                                        name="nama_pengelola"
                                                        onChange={this.handleFieldChange}
                                                        className="form-control"
                                                        placeholder="Masukan nama pengelola"
                                                    />
                                                    <span className="text-danger">{this.state.errList.nama_pengelola}</span>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label for="alamat" className="col-md-3 col-form-label text-md-right">Alamat</label>
                                                <div className="col-md-8">
                                                    <input 
                                                        type="text"
                                                        name="alamat"
                                                        onChange={this.handleFieldChange} 
                                                        className="form-control"
                                                        placeholder="Masukan alamat saat ini"
                                                    />
                                                    <span className="text-danger">{this.state.errList.alamat}</span>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label for="jenis_kelamin" className="col-md-3 col-form-label text-md-right">Jenis kelamin</label>
                                                <div className="col-md-8">
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="jenis_kelamin" id="laki-laki" value="1" onChange={this.handleFieldChange} />
                                                        <label className="form-check-label" for="laki-laki">
                                                            Laki-laki
                                                        </label>
                                                    </div>
                                                    <div className="form-check form-check-inline">
                                                        <input className="form-check-input" type="radio" name="jenis_kelamin" id="perempuan" value="0" onChange={this.handleFieldChange}/>
                                                        <label className="form-check-label" for="perempuan">
                                                            Perempuan
                                                        </label>
                                                    </div>
                                                    <br></br>
                                                    <span className="text-danger">{this.state.errList.jenis_kelamin}</span>
                                                </div>
                                            </div>
                                            <div className="form-group row">
                                                <label for="no_hp_pengelola" className="col-md-3 col-form-label text-md-right">Nomor handphone</label>
                                                <div className="col-md-8">
                                                    <div className="input-group">
                                                        <div className="input-group-prepend">
                                                            <div className="input-group-text">+62 </div>
                                                        </div>
                                                        <input 
                                                            type="number" 
                                                            name="no_hp_pengelola"
                                                            onChange={this.handleFieldChange}
                                                            className="form-control"
                                                            min="1"
                                                            placeholder="8xx.."
                                                        />
                                                    </div>
                                                    <span className="text-danger">{this.state.errList.no_hp_pengelola}</span>
                                                </div>
                                            </div>
                                            
                                            <div className="form-group row">
                                                <div className="col-md-8 offset-md-3 mb-2">
                                                    <button type="submit" className="btn btn-success" onClick={this.handleSubmit} disabled={this.state.isLoading}>
                                                        {this.state.isLoading ? <i className="fas fa-spinner fa-pulse"></i> : <i className="fas fa-sign-in-alt"></i>} Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- Footer --> */}
                    <Footer/>
                    {/* <!-- End of Footer --> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default TambahPengelola;