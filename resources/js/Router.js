import React from "react";
import {  Switch, Route } from 'react-router-dom';
import { PrivateRoute } from './PrivateRoute';
import { WadirRoute } from './WadirRoute';
import { PengelolaRoute } from './PengelolaRoute';
import { MahasiswaRoute } from './MahasiswaRoute';

import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import Profile from './pages/Profile';

import ForgotPassword from './pages/ForgotPassword';
import ResetPassword from './pages/ResetPassword';

import NotFound from './pages/NotFound';

//Mahasiswa
import FormPresensi from './pages/FormPresensi';
import FormIzinPulang from './pages/FormIzinPulang';
import FormResign from './pages/FormResign';
import FormIzinKembali from "./pages/FormIzinKembali";
import RiwayatPresensi from './pages/RiwayatPresensi';
import RiwayatPerizinan from './pages/RiwayatPerizinan';

//Pengelola dan Wadir
import DataIzinPulang from './pages/DataIzinPulang';
import FormApprovalIzinPulang from './pages/FormApprovalIzinPulang';

import DataIzinKembali from './pages/DataIzinKembali';
import FormApprovalIzinKembali from "./pages/FormApprovalIzinKembali";

import DataResign from './pages/DataResign';
import FormApprovalResign from "./pages/FormApprovalResign";

import JadwalPresensi from './pages/JadwalPresensi';
import TambahJadwal from './pages/TambahJadwal';
import EditJadwal from './pages/EditJadwal';

import DataPresensi from './pages/DataPresensi';
import DetailPresensi from './pages/DetailPresensi';

import Rekapitulasi from './pages/Rekapitulasi';

//Wadir
import ImportMahasiswa from './pages/ImportMahasiswa';
import DataMahasiswa from './pages/DataMahasiswa';
import DetailMahasiswa from './pages/DetailMahasiswa';
import EditMahasiswa from './pages/EditMahasiswa';
import RiwayatMahasiswa from './pages/RiwayatMahasiswa';
import ArsipMahasiswa from './pages/ArsipMahasiswa';
import DataPengelola from './pages/DataPengelola';
import EditPengelola from './pages/EditPengelola';
import TambahMahasiswa from './pages/TambahMahasiswa';
import TambahPengelola from './pages/TambahPengelola';

const Main = props =>(
    <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/forgot-password" component={ForgotPassword} />
        <Route exact path="/reset-password/:token" component={ResetPassword} />

        <PrivateRoute exact path="/dashboard" component={Dashboard} />
        <MahasiswaRoute exact path="/data-diri" component={Profile} />
        
        <MahasiswaRoute exact path="/form-presensi" component={FormPresensi} />
        <MahasiswaRoute exact path="/form-izin-pulang" component={FormIzinPulang} />
        <MahasiswaRoute exact path="/form-resign" component={FormResign} />
        <MahasiswaRoute exact path="/form-izin-kembali/:id" component={FormIzinKembali} />
        <MahasiswaRoute exact path="/riwayat-presensi" component={RiwayatPresensi} />
        <MahasiswaRoute exact path="/riwayat-perizinan" component={RiwayatPerizinan} />

        <PengelolaRoute exact path="/data-izin-pulang" component={DataIzinPulang} />
        <PengelolaRoute exact path="/form-approval-izin-pulang/:id" component={FormApprovalIzinPulang} />

        <PengelolaRoute exact path="/data-izin-kembali" component={DataIzinKembali} />
        <PengelolaRoute exact path="/form-approval-izin-kembali/:id" component={FormApprovalIzinKembali} />

        <PengelolaRoute exact path="/data-resign" component={DataResign} />
        <PengelolaRoute exact path="/form-approval-resign/:id" component={FormApprovalResign} />
        
        
        <PengelolaRoute exact path="/jadwal-presensi" component={JadwalPresensi} />
        <PengelolaRoute exact path="/tambah-jadwal" component={TambahJadwal} />
        <PengelolaRoute exact path="/edit-jadwal/:id" component={EditJadwal} />
        
        <PengelolaRoute exact path="/data-presensi" component={DataPresensi} />
        <PengelolaRoute exact path="/detail-presensi/:id" component={DetailPresensi} />

        <PengelolaRoute exact path="/rekapitulasi" component={Rekapitulasi} />

        <PengelolaRoute exact path="/edit-pengelola/:id" component={EditPengelola} />

        <WadirRoute exact path="/import-mahasiswa" component={ImportMahasiswa} />
        <WadirRoute exact path="/data-mahasiswa" component={DataMahasiswa} />
        <WadirRoute exact path="/detail-mahasiswa/:id" component={DetailMahasiswa} />
        <WadirRoute exact path="/edit-mahasiswa/:id" component={EditMahasiswa} />
        <WadirRoute exact path="/tambah-mahasiswa" component={TambahMahasiswa} />
        <WadirRoute exact path="/arsip-mahasiswa" component={ArsipMahasiswa} />
        <WadirRoute exact path="/riwayat-mahasiswa/:id" component={RiwayatMahasiswa} />
        <WadirRoute exact path="/data-pengelola" component={DataPengelola} />
        <WadirRoute exact path="/tambah-pengelola" component={TambahPengelola} />
        
        <Route path="*" component={NotFound} />
    </Switch>
)

export default Main;