<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJadwalPresensi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_presensi', function (Blueprint $table) {
            $table->bigIncrements('id_jadwal_presensi');
            $table->string('nama_jadwal', 20);
            $table->time('jam_mulai');
            $table->time('jam_selesai');
            $table->tinyInteger('status_jadwal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_presensi');
    }
}
