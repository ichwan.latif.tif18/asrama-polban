<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Pengelola;

class PengelolaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'email' => "rizqa.fauziyyah.tif18@polban.ac.id",
            'password' => \Hash::make(181511065),
            'role' => 2
        ]);

        $pengelola = Pengelola::create([
            'id_users' => $user->id_users,
            'nama_pengelola' => "Rizqa Fauziyyah",
            'jenis_kelamin' => 0,
            'alamat' => "jl. Tasik",
            'no_hp_pengelola' => "0812345678"
        ]);
    }
}
