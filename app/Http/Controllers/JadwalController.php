<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JadwalPresensi;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class JadwalController extends Controller
{
    public function store(Request $request){
        $messages = [
            'required'          => ':attribute harus diisi. '
        ];

        $validasi = \Validator::make($request->all(), [
            'nama_jadwal' => 'required',
            'jam_mulai' => 'required',
            'jam_selesai' => 'required',
        ], $messages);

        if($validasi->fails()){
            return response()->json(["status" => "error", "message" => $validasi->errors()]);
        }

        $jadwal = DB::table('jadwal_presensi')
        ->whereBetween('jam_mulai', [$request->jam_mulai, $request->jam_selesai])
        ->orWhereBetween('jam_selesai', [$request->jam_mulai, $request->jam_selesai])
        ->orWhere(function($q) use ($request){
            $q->where('jam_mulai', '<', $request->jam_mulai)
            ->where('jam_selesai', '>', $request->jam_selesai);
        })
        ->get();

        if($jadwal->count() > 0)
        {
            return response()->json([
                'status' => 'error',
                'message' => 'Waktu jadwal tidak valid',
            ]);
        }

        $insert = JadwalPresensi::create([
            'nama_jadwal' => $request->nama_jadwal,
            'jam_mulai' => $request->jam_mulai,
            'jam_selesai' => $request->jam_selesai,
            'status_jadwal' => 1,
        ]);

        if($insert){
            return response()->json([
                'status' => 'success',
                'message' => 'Jadwal berhasil diinput',
                'data' => $insert
            ]);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Jadwal gagal diinput',
            ]);
        }
    }

    public function update(Request $request){
        $messages = [
            'required'          => ':attribute harus diisi. '
        ];

        $validasi = \Validator::make($request->all(), [
            'nama_jadwal' => 'required',
            'jam_mulai' => 'required',
            'jam_selesai' => 'required',
        ], $messages);

        if($validasi->fails()){
            return response()->json(["status" => "error", "message" => $validasi->errors()]);
        }
        $update = JadwalPresensi::where('id_jadwal_presensi', '=', $request->id_jadwal_presensi)
        ->update([
            'nama_jadwal' => $request->nama_jadwal,
            'jam_mulai' => $request->jam_mulai,
            'jam_selesai' => $request->jam_selesai,
            'status_jadwal' => 1,
        ]);

        if($update){
            return response()->json([
                'status' => 'success',
                'message' => 'Jadwal berhasil diupdate',
                'data' => $update
            ]);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Jadwal gagal diinput',
            ]);
        }
    }

    public function delete(Request $request){
        $listJadwal = $request->listJadwal;
        for($i=0; $i < count($listJadwal); $i++){

            $delete = JadwalPresensi::where([
                ['id_jadwal_presensi', '=', $listJadwal[$i]],
            ])
            ->delete();
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Mahasiswa berhasil dihapus',
            'data' => $delete
        ], 201);
    }

    public function getAllJadwal(){
        $jadwal = DB::table('jadwal_presensi')
        ->orderBy('jam_mulai', 'asc')
        ->get();

        if($jadwal){
            return response()->json([
                "status" => 'success',
                "message" => "Success get jadwal",
                "data" => $jadwal
            ]);
        }
        else{
            return response()->json([
                "status" => 'error',
                "message" => "Jadwal Not Found"
            ]);
        }
    }

    public function getJadwalById($id){
        $jadwal = DB::table('jadwal_presensi')
        ->where('id_jadwal_presensi', $id)
        ->first();

        if($jadwal){
            return response()->json([
                "status" => 'success',
                "message" => "Success get jadwal",
                "data" => $jadwal
            ]);
        }
        else{
            return response()->json([
                "status" => 'error',
                "message" => "Jadwal Not Found"
            ]);
        }
    }

    public function getCurrentJadwal(){
        $currentTime = Carbon::now()->format('H:i:s');
        $jadwal = DB::table('jadwal_presensi')
        ->where([
            [ 'jam_mulai', '<=', $currentTime],
            [ 'jam_selesai', '>=', $currentTime]
        ])
        ->first();

        if($jadwal){
            return response()->json([
                "status" => 'success',
                "message" => "Success get jadwal",
                "data" => $jadwal
            ]);
        }
        else{
            return response()->json([
                "status" => 'error',
                "message" => "Jadwal Not Found"
            ]);
        }
    }
}
