<?php

namespace App\Http\Controllers;

use App\Models\Pengelola;
use App\Models\User;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PengelolaController extends Controller
{
    public function store(Request $request){
        $messages = [
            'required'          => ':attribute harus diisi. ',
            'email'             => ':attribute tidak valid. ',
            'ends_with'         => 'harus menggunakan :attribute polban. ',
            'numeric'           => ':attribute harus diisi angka. ',
            'max'               => ':attribute harus diisi maksimal :max. ',
            'min'               => ':attribute harus diisi minimal :min. ',
        ];

        $validasi = \Validator::make($request->all(), [
            'email' => 'required|email|ends_with:polban.ac.id',
            'password' => 'required|alpha_num|min:8',
            'nama_pengelola' => 'required|max:50', 
            'jenis_kelamin' => 'required',
            'alamat' => 'required|max:125',
            'no_hp_pengelola' => 'required|numeric'
        ], $messages);

        if($validasi->fails()){
            return response()->json(["status" => "error", "message" => $validasi->errors()]);
        }

        $user = User::create([
            'email' => $request->email,
            'password' => \Hash::make($request->password),
            'role' => 2,
        ]);

        $insert = Pengelola::create([
            'id_users' => $user->id_users,
            'nama_pengelola' => strtoupper($request->nama_pengelola), 
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'no_hp_pengelola' => '0' . $request->no_hp_pengelola
        ]);

        if($insert){
            return response()->json([
                'status' => 'success',
                'message' => 'Pengelola berhasil diinput',
                'data' => $insert
            ]);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Pengelola gagal diinput',
            ]);
        }
    }

    public function update(Request $request){
        $messages = [
            'required'          => ':attribute harus diisi. ',
            'email'             => ':attribute tidak valid. ',
            'ends_with'         => 'harus menggunakan :attribute polban. ',
            'numeric'           => ':attribute harus diisi angka. ',
            'max'               => ':attribute harus diisi maksimal :max. ',
            'min'               => ':attribute harus diisi minimal :min. ',
        ];

        if($request->password == "" || $request->password == null){
            $validasi = \Validator::make($request->all(), [
                'email' => 'required|email|ends_with:polban.ac.id',
                'nama_pengelola' => 'required|max:50', 
                'jenis_kelamin' => 'required',
                'alamat' => 'required|max:125',
                'no_hp_pengelola' => 'required|numeric'
            ], $messages);
        }else{
            $validasi = \Validator::make($request->all(), [
                'email' => 'required|email|ends_with:polban.ac.id',
                'password' => 'required|alpha_num|min:8',
                'nama_pengelola' => 'required|max:50', 
                'jenis_kelamin' => 'required',
                'alamat' => 'required|max:125',
                'no_hp_pengelola' => 'required|numeric'
            ], $messages);
        }

        if($validasi->fails()){
            return response()->json(["status" => "error", "message" => $validasi->errors()]);
        }

        $pengelola = Pengelola::where([
            ['id_pengelola', '=', $request->id_pengelola],
        ]);

        if($request->password == "" || $request->password == null){
            $user = User::where('id_users', '=',$request->id_users)
            ->update([
                'email' => $request->email,
                'role' => 2,
            ]);
        }else{
            $user = User::where('id_users', $request->id_users)
            ->update([
                'email' => $request->email,
                'password' => \Hash::make($request->password),
                'role' => 2,
            ]);
        }

        $update = $pengelola->update([
            'nama_pengelola' => strtoupper($request->nama_pengelola), 
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'no_hp_pengelola' => '0' . $request->no_hp_pengelola
        ]);

        if($update){
            return response()->json([
                'status' => 'success',
                'message' => 'Pengelola berhasil diupdate',
                'data' => $update
            ], 201);
        }
        else{
            return response()->json([
                'status' => 'error',
                'message' => 'Pengelola gagal diiupdate',
            ]);
        }
    }

    public function delete(Request $request){
        $listPengelola = $request->listPengelola;
        for($i=0; $i < count($listPengelola); $i++){
            $id_user = Pengelola::where([
                ['id_pengelola', '=',$listPengelola[$i]],
            ])->first()->id_users;

            $delete = Pengelola::where([
                ['id_pengelola', '=', $listPengelola[$i]],
            ])->delete();

            User::where([
                ['id_users', '=', $id_user],
            ])
            ->delete();
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Pengelola berhasil dihapus',
            'data' => $delete
        ], 201);
    }

    public function getAllPengelola(){
        $pengelola = DB::table('pengelola')
        ->join('users', 'pengelola.id_users', '=', 'users.id_users')
        ->orderBy('pengelola.nama_pengelola', 'asc')
        ->get();

        if($pengelola){
            return response()->json([
                "status" => 'success',
                "message" => "Success get pengelola",
                "data" => $pengelola
            ]);
        }
        else{
            return response()->json([
                "status" => 'error',
                "message" => "Pengelola Not Found"
            ]);
        }
    }
    
    public function getPengelolaById($id){
        $pengelola = DB::table('pengelola')
        ->where([['id_pengelola', '=', $id]])
        ->join('users', 'pengelola.id_users', '=', 'users.id_users')
        ->first();
        
        if($pengelola){
            return response()->json([
                "status" => 'success',
                "message" => "Success get pengelola",
                "data" => $pengelola
            ]);
        }
        else{
            return response()->json([
                "status" => 'error',
                "message" => "Pengelola Not Found"
            ]);
        }
        
    }

    public function getPengelolaByUserId($id){
        $pengelola = DB::table('pengelola')
        ->where([['id_users', '=', $id]])
        ->first();
        
        if($pengelola){
            return response()->json([
                "status" => 'success',
                "message" => "Success get pengelola",
                "data" => $pengelola
            ]);
        }
        else{
            return response()->json([
                "status" => 'error',
                "message" => "Pengelola Not Found"
            ]);
        }
        
    }
}
