<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Models\Presensi;
use App\Models\Mahasiswa;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class RekapitulasiExport implements FromCollection, WithHeadings
{
    use Exportable;

    public function __construct($date_from, $date_to){
        $this->date_from = $date_from;
        $this->date_to = $date_to;
    }

    public function headings(): array
    {
        return [
            'NAMA',
            'NIM',
            'NO KAMAR',
            'GEDUNG',
            'KETERANGAN ASAL',
            'ROLE',
            'ALFA',
            'IZIN',
            'HADIR'
        ];
    }

    public function collection()
    {
        $rekap_mahasiswas = new Collection;
        $rekap_mahasiswa = new Collection;
        $mahasiswas = DB::table('mahasiswa')
        ->where([['status_keaktifan', '=', 1]])
        ->join('kamar', 'mahasiswa.id_kamar', '=', 'kamar.id_kamar')
        ->join('gedung', 'kamar.id_gedung', '=', 'gedung.id_gedung')
        ->orderBy('kamar.id_kamar', 'asc')
        ->orderBy('mahasiswa.nama_mhs', 'asc')
        ->get();
        foreach($mahasiswas as $mahasiswa){
            $presensi = Presensi::where([
                ['id_mhs', '=', $mahasiswa->id_mhs],
            ])
            ->where([
                ['created_at', '<=', $this->date_to],
                ['created_at', '>=', $this->date_from]
            ])
            ->get();
            
            $alfa = $presensi->where('status_presensi', '=', 0)->count();
            $hadir = $presensi->where('status_presensi', '=', 1)->count();
            $izin = $presensi->where('status_presensi', '=', 2)->count();

            $resign = DB::table('resign')
            ->where('id_mhs', '=', $mahasiswa->id_mhs)
            ->where('status_resign', '=', 1)
            ->first();
            $rekap_mahasiswa = collect([
                'nama_mhs' => $mahasiswa->nama_mhs, 
                'nim' => $mahasiswa->nim, 
                'no_kamar' => $mahasiswa->no_kamar, 
                'nama_gedung' => $mahasiswa->nama_gedung,
                'keterangan_asal' => $mahasiswa->keterangan_asal,
                'role_mhs' => $mahasiswa->role_mhs, 
                'alfa' => $alfa, 
                'hadir' => $hadir, 
                'izin' => $izin
            ]);
            $rekap_mahasiswas->push($rekap_mahasiswa);
        }

        return $rekap_mahasiswas;
    }
}
