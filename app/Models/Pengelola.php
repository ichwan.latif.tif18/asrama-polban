<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengelola extends Model
{
    use HasFactory;
    protected $table = 'pengelola';

    protected $fillable = [
        'id_users',
        'nama_pengelola',
        'jenis_kelamin',
        'alamat',
        'no_hp_pengelola'
    ];

    protected $primaryKey = 'id_pengelola';
}
