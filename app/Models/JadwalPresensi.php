<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JadwalPresensi extends Model
{
    use HasFactory;
    protected $table = 'jadwal_presensi';

    protected $fillable = [
        'nama_jadwal',
        'jam_mulai',
        'jam_selesai',
        'status_jadwal'
    ];

    protected $primaryKey = 'id_jadwal_presensi';
}
