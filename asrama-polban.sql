-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2021 at 12:45 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asrama-polban`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gedung`
--

CREATE TABLE `gedung` (
  `id_gedung` bigint(20) UNSIGNED NOT NULL,
  `nama_gedung` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gedung`
--

INSERT INTO `gedung` (`id_gedung`, `nama_gedung`, `created_at`, `updated_at`) VALUES
(1, 'A', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(2, 'B', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(3, 'C', '2021-09-22 03:43:19', '2021-09-22 03:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_presensi`
--

CREATE TABLE `jadwal_presensi` (
  `id_jadwal_presensi` bigint(20) UNSIGNED NOT NULL,
  `nama_jadwal` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `status_jadwal` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` bigint(20) UNSIGNED NOT NULL,
  `nama_jurusan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `nama_jurusan`, `created_at`, `updated_at`) VALUES
(1, 'Teknik Sipil', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(2, 'Teknik Mesin', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(3, 'Teknik Refrigasi dan Tata Udara', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(4, 'Teknik Konversi Energi', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(5, 'Teknik Elektro', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(6, 'Teknik Kimia', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(7, 'Teknik Komputer dan Informatika', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(8, 'Akuntansi', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(9, 'Administrasi Niaga', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(10, 'Bahasa Inggris', '2021-09-22 03:43:19', '2021-09-22 03:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `kamar`
--

CREATE TABLE `kamar` (
  `id_kamar` bigint(20) UNSIGNED NOT NULL,
  `id_gedung` bigint(20) UNSIGNED NOT NULL,
  `no_kamar` int(11) NOT NULL,
  `lantai` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kamar`
--

INSERT INTO `kamar` (`id_kamar`, `id_gedung`, `no_kamar`, `lantai`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(2, 1, 2, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(3, 1, 3, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(4, 1, 4, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(5, 1, 5, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(6, 1, 6, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(7, 1, 7, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(8, 1, 8, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(9, 1, 9, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(10, 1, 10, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(11, 1, 11, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(12, 1, 12, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(13, 1, 13, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(14, 1, 14, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(15, 1, 15, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(16, 1, 16, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(17, 1, 17, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(18, 1, 18, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(19, 1, 19, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(20, 1, 20, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(21, 1, 21, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(22, 1, 22, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(23, 1, 23, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(24, 1, 24, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(25, 2, 1, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(26, 2, 2, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(27, 2, 3, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(28, 2, 4, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(29, 2, 5, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(30, 2, 6, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(31, 2, 7, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(32, 2, 8, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(33, 2, 9, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(34, 2, 10, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(35, 2, 11, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(36, 2, 12, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(37, 2, 13, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(38, 2, 14, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(39, 2, 15, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(40, 2, 16, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(41, 2, 17, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(42, 2, 18, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(43, 2, 19, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(44, 2, 20, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(45, 2, 21, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(46, 2, 22, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(47, 2, 23, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(48, 2, 24, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(49, 3, 1, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(50, 3, 2, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(51, 3, 3, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(52, 3, 4, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(53, 3, 5, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(54, 3, 6, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(55, 3, 7, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(56, 3, 8, 1, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(57, 3, 9, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(58, 3, 10, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(59, 3, 11, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(60, 3, 12, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(61, 3, 13, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(62, 3, 14, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(63, 3, 15, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(64, 3, 16, 2, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(65, 3, 17, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(66, 3, 18, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(67, 3, 19, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(68, 3, 20, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(69, 3, 21, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(70, 3, 22, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(71, 3, 23, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(72, 3, 24, 3, '2021-09-22 03:43:20', '2021-09-22 03:43:20');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mhs` bigint(20) UNSIGNED NOT NULL,
  `id_users` bigint(20) UNSIGNED NOT NULL,
  `id_prodi` bigint(20) UNSIGNED NOT NULL,
  `id_kamar` bigint(20) UNSIGNED NOT NULL,
  `nama_mhs` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nim` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp_mhs` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_ortu` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp_ortu` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` tinyint(4) NOT NULL,
  `status_keaktifan` tinyint(4) NOT NULL DEFAULT 1,
  `tanggal_lahir` date NOT NULL,
  `agama` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan_asal` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_mhs` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id_mhs`, `id_users`, `id_prodi`, `id_kamar`, `nama_mhs`, `nim`, `alamat`, `no_hp_mhs`, `nama_ortu`, `no_hp_ortu`, `jenis_kelamin`, `status_keaktifan`, `tanggal_lahir`, `agama`, `keterangan_asal`, `role_mhs`, `created_at`, `updated_at`) VALUES
(1, 1, 23, 25, 'Ichwan Latif Fajari', '181511046', 'Perum Villa Permata Blok DD5/33', '08991276549', 'Pak Chibe', '087878910454', 1, 1, '2000-04-17', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(2, 2, 2, 46, 'Faizah Febi Hastuti', '181511038', 'Dk. Bak Mandi No. 937, Administrasi Jakarta Timur 24606, Banten', '0812345678', 'Genta Mutia Susanti', '0812345678', 1, 0, '2014-01-29', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(3, 3, 2, 29, 'Jail Prabowo S.T.', '181511030', 'Gg. Taman No. 6, Semarang 37112, Sulsel', '0812345678', 'Queen Kania Pertiwi S.E.', '0812345678', 0, 1, '2006-08-30', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(4, 4, 2, 65, 'Bambang Candrakanta Lazuardi', '181511062', 'Dk. Sumpah Pemuda No. 973, Bontang 60113, Sumsel', '0812345678', 'Ian Adika Irawan M.Farm', '0812345678', 0, 1, '2015-09-10', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(5, 5, 34, 71, 'Tiara Haryanti', '181511046', 'Jln. Fajar No. 902, Padangsidempuan 53226, DIY', '0812345678', 'Victoria Yuniar', '0812345678', 1, 0, '1971-06-22', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(6, 6, 19, 70, 'Halim Narpati', '181511060', 'Ds. PHH. Mustofa No. 579, Manado 29703, Lampung', '0812345678', 'Mulyanto Latupono', '0812345678', 0, 1, '1992-08-11', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(7, 7, 13, 7, 'Hesti Padmasari', '181511039', 'Ds. Bayam No. 156, Surabaya 66697, Jatim', '0812345678', 'Hafshah Namaga', '0812345678', 1, 1, '2012-01-11', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(8, 8, 19, 20, 'Atmaja Sihombing', '181511050', 'Dk. Haji No. 273, Yogyakarta 52067, Aceh', '0812345678', 'Laras Nuraini', '0812345678', 1, 1, '2017-11-10', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(9, 9, 26, 64, 'Diana Vivi Hariyah', '181511050', 'Kpg. Gegerkalong Hilir No. 137, Padangpanjang 67946, Sulteng', '0812345678', 'Ana Aryani', '0812345678', 0, 0, '2005-02-24', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(10, 10, 28, 13, 'Karimah Farida', '181511005', 'Gg. Sugiono No. 474, Banjarbaru 95167, Kepri', '0812345678', 'Ifa Jamalia Puspasari', '0812345678', 0, 0, '1979-03-02', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(11, 11, 13, 44, 'Pandu Jayadi Megantara', '181511028', 'Psr. Babadan No. 46, Balikpapan 85030, Kalbar', '0812345678', 'Puji Susanti', '0812345678', 0, 1, '2008-08-19', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(12, 12, 29, 5, 'Hani Zelda Yuliarti S.Pd', '181511013', 'Gg. Muwardi No. 809, Ambon 80112, Kaltara', '0812345678', 'Yuni Salsabila Astuti', '0812345678', 1, 0, '2012-02-02', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(13, 13, 29, 42, 'Gina Unjani Mulyani', '181511002', 'Psr. Padma No. 6, Medan 48333, Kaltim', '0812345678', 'Sabrina Kezia Uyainah M.M.', '0812345678', 0, 0, '1990-09-08', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(14, 14, 26, 34, 'Vera Nadia Wijayanti S.Pt', '181511010', 'Ki. Urip Sumoharjo No. 771, Payakumbuh 40830, Maluku', '0812345678', 'Ida Fujiati', '0812345678', 1, 0, '2009-10-12', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(15, 15, 13, 4, 'Aurora Padmasari', '181511010', 'Ds. Rajawali No. 163, Pekalongan 52678, Bengkulu', '0812345678', 'Tasdik Zulkarnain', '0812345678', 1, 1, '1976-06-25', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(16, 16, 36, 43, 'Humaira Mardhiyah', '181511042', 'Jln. Bahagia No. 751, Tangerang Selatan 44749, NTT', '0812345678', 'Yani Prastuti', '0812345678', 0, 1, '1974-09-29', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(17, 17, 34, 36, 'Ira Kuswandari', '181511021', 'Dk. Nanas No. 894, Payakumbuh 53532, Kepri', '0812345678', 'Siska Indah Kuswandari S.IP', '0812345678', 0, 1, '2020-12-04', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(18, 18, 17, 69, 'Opan Marbun', '181511006', 'Dk. Salam No. 262, Lubuklinggau 22429, DIY', '0812345678', 'Kurnia Kacung Hutagalung', '0812345678', 1, 0, '1989-10-29', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(19, 19, 16, 4, 'Fitria Novitasari', '181511050', 'Dk. Bakit  No. 370, Gorontalo 77491, Sulteng', '0812345678', 'Eman Embuh Thamrin S.Kom', '0812345678', 1, 0, '2021-02-25', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(20, 20, 14, 59, 'Olga Setiawan', '181511021', 'Gg. Ters. Kiaracondong No. 595, Payakumbuh 30887, Jabar', '0812345678', 'Gandi Jaka Kusumo', '0812345678', 0, 0, '1997-08-02', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(21, 21, 16, 11, 'Dewi Restu Hasanah S.Farm', '181511039', 'Ds. Sentot Alibasa No. 35, Tangerang 49334, Kaltara', '0812345678', 'Aslijan Najmudin', '0812345678', 0, 0, '1971-07-26', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(22, 22, 36, 45, 'Carla Padmi Mulyani S.E.', '181511051', 'Jr. Agus Salim No. 346, Kotamobagu 25750, Sumbar', '0812345678', 'Cakrajiya Nugroho', '0812345678', 1, 0, '1982-06-16', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(23, 23, 6, 10, 'Lanjar Prasasta', '181511022', 'Ki. Villa No. 669, Administrasi Jakarta Selatan 70167, Papua', '0812345678', 'Citra Laksmiwati', '0812345678', 0, 1, '1998-04-07', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(24, 24, 26, 55, 'Bakiman Wahyudin', '181511060', 'Psr. Bappenas No. 756, Administrasi Jakarta Timur 19500, Sulteng', '0812345678', 'Dina Haryanti M.Ak', '0812345678', 1, 1, '2010-06-07', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(25, 25, 7, 6, 'Jaeman Ade Tarihoran S.Gz', '181511064', 'Ds. Nangka No. 640, Jambi 29197, Sulbar', '0812345678', 'Yuliana Nuraini', '0812345678', 0, 0, '1975-12-02', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(26, 26, 29, 22, 'Uli Winarsih', '181511044', 'Kpg. Kiaracondong No. 484, Balikpapan 42883, Papua', '0812345678', 'Taufik Reza Rajasa', '0812345678', 1, 0, '2012-03-27', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(27, 27, 21, 29, 'Pardi Ghani Kuswoyo S.Pd', '181511025', 'Gg. Abdullah No. 518, Palembang 25838, Jambi', '0812345678', 'Makara Situmorang', '0812345678', 1, 1, '2017-07-23', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(28, 28, 10, 36, 'Murti Anggriawan', '181511030', 'Ds. Peta No. 63, Semarang 31265, Banten', '0812345678', 'Sakti Budiyanto M.Pd', '0812345678', 0, 0, '2019-11-25', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(29, 29, 22, 43, 'Rina Puspita', '181511045', 'Dk. Hang No. 996, Palangka Raya 73044, Maluku', '0812345678', 'Tiara Farida', '0812345678', 1, 0, '1978-11-13', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(30, 30, 31, 8, 'Okta Habibi', '181511055', 'Gg. Yoga No. 12, Tual 93106, NTB', '0812345678', 'Jamalia Zulaika', '0812345678', 1, 1, '2008-08-19', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(31, 31, 34, 36, 'Darman Prasetyo', '181511050', 'Ki. Raden Saleh No. 687, Makassar 81992, Kalbar', '0812345678', 'Enteng Halim', '0812345678', 0, 1, '1976-06-09', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(32, 32, 35, 3, 'Raisa Rahimah', '181511015', 'Jr. Tambak No. 640, Serang 90409, Sumbar', '0812345678', 'Queen Riyanti', '0812345678', 1, 0, '2020-06-05', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(33, 33, 22, 14, 'Danu Lazuardi', '181511062', 'Ds. Aceh No. 921, Tomohon 22945, Babel', '0812345678', 'Jaeman Indra Suryono', '0812345678', 1, 1, '2006-04-02', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(34, 34, 2, 42, 'Mursita Mangunsong', '181511035', 'Jr. Bayam No. 151, Bontang 69198, Sumsel', '0812345678', 'Ophelia Hariyah', '0812345678', 0, 0, '1983-04-30', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(35, 35, 19, 48, 'Muhammad Prasetyo', '181511002', 'Ds. Bakhita No. 790, Kotamobagu 25723, Maluku', '0812345678', 'Lalita Puspita', '0812345678', 1, 1, '1975-10-09', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(36, 36, 32, 18, 'Labuh Saefullah', '181511045', 'Dk. B.Agam Dlm No. 357, Bitung 32794, Lampung', '0812345678', 'Estiono Galiono Salahudin S.Ked', '0812345678', 0, 0, '2010-08-03', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(37, 37, 31, 5, 'Anastasia Anita Rahimah', '181511032', 'Gg. Ters. Kiaracondong No. 226, Banjarmasin 20975, DIY', '0812345678', 'Unjani Safitri', '0812345678', 1, 1, '1990-12-09', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(38, 38, 18, 27, 'Mahesa Wacana', '181511063', 'Ds. Perintis Kemerdekaan No. 651, Batu 88908, Sulbar', '0812345678', 'Darman Prayoga', '0812345678', 0, 1, '1985-04-03', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(39, 39, 33, 36, 'Ozy Januar', '181511045', 'Jr. Babadak No. 381, Pangkal Pinang 92263, Bengkulu', '0812345678', 'Tantri Halimah', '0812345678', 0, 1, '1976-11-18', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(40, 40, 16, 38, 'Legawa Mansur', '181511051', 'Ds. Sudirman No. 642, Surabaya 35714, Lampung', '0812345678', 'Baktianto Saefullah', '0812345678', 1, 0, '1988-08-18', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(41, 41, 18, 30, 'Kamaria Genta Farida', '181511028', 'Psr. Baha No. 613, Pontianak 88611, Jabar', '0812345678', 'Titin Kusmawati', '0812345678', 0, 1, '1985-12-23', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(42, 42, 33, 22, 'Mumpuni Prabowo S.Farm', '181511002', 'Gg. Baung No. 538, Semarang 84870, DKI', '0812345678', 'Harjo Nashiruddin M.Farm', '0812345678', 0, 1, '2002-05-15', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(43, 43, 27, 53, 'Almira Hariyah S.E.', '181511028', 'Jr. Camar No. 296, Batam 94828, DKI', '0812345678', 'Siti Tira Padmasari S.E.I', '0812345678', 1, 1, '1990-07-07', 'Islam', 'ADIK', 'Pengurus', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(44, 44, 2, 60, 'Vino Najmudin S.Pd', '181511067', 'Ds. Fajar No. 671, Pagar Alam 54589, Riau', '0812345678', 'Cemani Situmorang', '0812345678', 0, 1, '2016-08-05', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(45, 45, 36, 64, 'Lanjar Nababan', '181511002', 'Ds. Kiaracondong No. 471, Mojokerto 50143, Kaltara', '0812345678', 'Garang Marpaung', '0812345678', 1, 1, '1971-05-07', 'Islam', 'KIP Kuliah', 'Pengurus', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(46, 46, 19, 55, 'Cinta Paramita Namaga M.Kom.', '181511016', 'Ds. Untung Suropati No. 532, Tangerang 79156, Gorontalo', '0812345678', 'Martani Samosir', '0812345678', 0, 0, '2001-02-03', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(47, 47, 9, 42, 'Latif Natsir S.H.', '181511050', 'Jln. Eka No. 693, Surakarta 69902, Gorontalo', '0812345678', 'Muni Widodo S.T.', '0812345678', 1, 1, '2008-06-16', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(48, 48, 25, 50, 'Bambang Setiawan', '181511056', 'Dk. Cut Nyak Dien No. 634, Depok 19339, Maluku', '0812345678', 'Jelita Dina Lailasari', '0812345678', 1, 0, '2012-03-31', 'Islam', 'ADIK', 'Mahasiswa', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(49, 49, 9, 52, 'Putri Ratna Usada', '181511064', 'Psr. K.H. Wahid Hasyim (Kopo) No. 567, Gorontalo 84821, Kalbar', '0812345678', 'Nova Novitasari', '0812345678', 0, 0, '2020-11-30', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(50, 50, 11, 62, 'Johan Hendri Sinaga', '181511016', 'Ds. Salam No. 938, Langsa 34342, Jateng', '0812345678', 'Ellis Novitasari', '0812345678', 1, 1, '1970-07-24', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(51, 51, 30, 54, 'Sarah Paulin Haryanti', '181511008', 'Dk. Daan No. 306, Balikpapan 54615, Riau', '0812345678', 'Irnanto Saadat Pangestu S.T.', '0812345678', 1, 1, '1999-06-04', 'Islam', 'KIP Kuliah', 'Mahasiswa', '2021-09-22 03:43:24', '2021-09-22 03:43:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2021_05_26_075122_create_gedung', 1),
(6, '2021_05_26_075123_create_jurusan', 1),
(7, '2021_05_26_075543_create_kamar', 1),
(8, '2021_05_26_075604_create_prodi', 1),
(9, '2021_05_26_075903_create_mahasiswa', 1),
(10, '2021_05_26_080103_create_presensi', 1),
(11, '2021_05_26_080123_create_perizinan', 1),
(12, '2021_05_26_080200_create_resign', 1),
(13, '2021_09_06_033428_create_jadwal_presensi', 1),
(14, '2021_09_06_034125_add_jadwal_in_presensi', 1),
(15, '2021_09_06_045324_create_pengelola', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengelola`
--

CREATE TABLE `pengelola` (
  `id_pengelola` bigint(20) UNSIGNED NOT NULL,
  `id_users` bigint(20) UNSIGNED NOT NULL,
  `nama_pengelola` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` tinyint(4) NOT NULL,
  `alamat` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp_pengelola` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengelola`
--

INSERT INTO `pengelola` (`id_pengelola`, `id_users`, `nama_pengelola`, `jenis_kelamin`, `alamat`, `no_hp_pengelola`, `created_at`, `updated_at`) VALUES
(1, 53, 'Rizqa Fauziyyah', 0, 'jl. Tasik', '0812345678', '2021-09-22 03:43:25', '2021-09-22 03:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `perizinan`
--

CREATE TABLE `perizinan` (
  `id_perizinan` bigint(20) UNSIGNED NOT NULL,
  `id_mhs` bigint(20) UNSIGNED NOT NULL,
  `tanggal_pergi` date NOT NULL,
  `tanggal_pulang` date DEFAULT NULL,
  `pengajuan_tanggal_pulang` date DEFAULT NULL,
  `keterangan_izin` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_izin` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan_kembali` varchar(125) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surat_pendukung` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan_approval` varchar(125) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_izin` tinyint(4) NOT NULL,
  `suhu_badan` double(8,2) NOT NULL,
  `kondisi_kesehatan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kendaraan` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `perizinan`
--

INSERT INTO `perizinan` (`id_perizinan`, `id_mhs`, `tanggal_pergi`, `tanggal_pulang`, `pengajuan_tanggal_pulang`, `keterangan_izin`, `alamat_izin`, `keterangan_kembali`, `surat_pendukung`, `catatan_approval`, `status_izin`, `suhu_badan`, `kondisi_kesehatan`, `jenis_kendaraan`, `created_at`, `updated_at`) VALUES
(1, 2, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Kpg. Ikan No. 173, Sibolga 10814, Maluku', NULL, NULL, NULL, 0, 36.70, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(2, 3, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Psr. Ters. Jakarta No. 819, Pagar Alam 17583, Sumut', NULL, NULL, NULL, 0, 36.90, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(3, 4, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Kpg. Mulyadi No. 47, Serang 26307, Sultra', NULL, NULL, NULL, 0, 36.70, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(4, 5, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Ds. Babadan No. 28, Banjarbaru 33212, Riau', NULL, NULL, NULL, 2, 37.30, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(5, 6, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Psr. Cikutra Timur No. 821, Prabumulih 95119, Jabar', NULL, NULL, NULL, 0, 36.90, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(6, 7, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Gg. Basuki No. 407, Sabang 43819, Lampung', NULL, NULL, NULL, 0, 37.50, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(7, 8, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Psr. Bakin No. 903, Probolinggo 55072, Sumbar', NULL, NULL, NULL, 3, 37.00, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(8, 9, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Gg. Kyai Mojo No. 532, Manado 77861, Sumbar', NULL, NULL, NULL, 0, 37.00, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(9, 10, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Ds. Kebonjati No. 793, Palangka Raya 96236, Maluku', NULL, NULL, NULL, 4, 37.20, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(10, 11, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Kpg. Padma No. 784, Gorontalo 99931, Sultra', NULL, NULL, NULL, 3, 37.00, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(11, 12, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Jln. Yoga No. 739, Tebing Tinggi 46881, NTB', NULL, NULL, NULL, 4, 36.50, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(12, 13, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Dk. Bahagia  No. 883, Parepare 87197, Kalbar', NULL, NULL, NULL, 4, 37.40, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(13, 14, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Jln. Taman No. 250, Bima 36035, Papua', NULL, NULL, NULL, 0, 36.60, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(14, 15, '2021-10-10', '2021-12-12', NULL, 'Pulkam', 'Ds. Basoka No. 90, Sukabumi 14555, Sumut', NULL, NULL, NULL, 0, 36.90, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(15, 1, '2021-10-10', '2021-12-12', NULL, 'Test pagination', 'Jln. Casablanca No. 825, Pematangsiantar 35728, Gorontalo', NULL, NULL, NULL, 0, 37.40, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(16, 1, '2021-10-10', '2021-12-12', NULL, 'Test pagination', 'Dk. Pacuan Kuda No. 903, Samarinda 41610, DKI', NULL, NULL, NULL, 1, 36.80, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(17, 1, '2021-10-10', '2021-12-12', NULL, 'Test pagination', 'Kpg. Ters. Kiaracondong No. 20, Cilegon 88416, Gorontalo', NULL, NULL, NULL, 2, 36.90, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(18, 1, '2021-10-10', '2021-12-12', NULL, 'Test pagination', 'Ds. Basudewo No. 205, Pekalongan 27223, Gorontalo', NULL, NULL, NULL, 0, 37.30, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(19, 1, '2021-10-10', '2021-12-12', NULL, 'Test pagination', 'Psr. Nangka No. 438, Ambon 22281, Malut', NULL, NULL, NULL, 1, 36.70, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(20, 16, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Kpg. Gambang No. 682, Administrasi Jakarta Pusat 45566, Bengkulu', 'Kuliah', NULL, NULL, 8, 37.00, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(21, 17, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Jr. Diponegoro No. 650, Padang 87944, Maluku', 'Kuliah', NULL, NULL, 8, 37.10, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(22, 18, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Ds. Moch. Ramdan No. 586, Tangerang 15871, Banten', 'Kuliah', NULL, NULL, 9, 37.30, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(23, 19, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Ki. S. Parman No. 186, Sawahlunto 17933, Kepri', 'Kuliah', NULL, NULL, 5, 37.20, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(24, 20, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Kpg. Cemara No. 700, Solok 32682, Sultra', 'Kuliah', NULL, NULL, 6, 37.20, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(25, 21, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Kpg. Basoka No. 923, Palopo 23124, Jatim', 'Kuliah', NULL, NULL, 5, 37.30, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(26, 22, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Gg. Halim No. 674, Bandar Lampung 57671, Kalteng', 'Kuliah', NULL, NULL, 8, 37.40, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(27, 23, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Ds. Muwardi No. 474, Tomohon 68483, Malut', 'Kuliah', NULL, NULL, 7, 37.30, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(28, 24, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Kpg. Reksoninten No. 197, Mataram 33785, Kalsel', 'Kuliah', NULL, NULL, 7, 37.00, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(29, 25, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Kpg. Casablanca No. 94, Pariaman 11658, Lampung', 'Kuliah', NULL, NULL, 9, 36.80, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(30, 26, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Dk. Gatot Subroto No. 937, Pekanbaru 67208, Babel', 'Kuliah', NULL, NULL, 5, 37.00, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(31, 27, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Jln. Raya Ujungberung No. 784, Pekanbaru 93414, Maluku', 'Kuliah', NULL, NULL, 8, 37.40, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(32, 28, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Jln. Sam Ratulangi No. 414, Sawahlunto 20400, Bali', 'Kuliah', NULL, NULL, 7, 37.50, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(33, 29, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Gg. Rajiman No. 496, Bandung 78697, Sulsel', 'Kuliah', NULL, NULL, 6, 36.90, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(34, 30, '2021-07-07', '2021-12-12', '2021-12-10', 'Pulkam', 'Kpg. Banal No. 597, Payakumbuh 42111, Pabar', 'Kuliah', NULL, NULL, 6, 36.90, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(35, 1, '2021-07-07', '2021-12-12', '2021-12-10', 'Test pagination', 'Ds. Muwardi No. 866, Pariaman 79384, Sumut', 'Test pagination', NULL, NULL, 8, 36.90, 'Sakit', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(36, 1, '2021-07-07', '2021-12-12', '2021-12-10', 'Test pagination', 'Dk. Wora Wari No. 317, Tomohon 64864, Sumbar', 'Test pagination', NULL, NULL, 8, 36.60, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(37, 1, '2021-07-07', '2021-12-12', '2021-12-10', 'Test pagination', 'Dk. Abdul No. 161, Pasuruan 56254, Bengkulu', 'Test pagination', NULL, NULL, 9, 36.70, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(38, 1, '2021-07-07', '2021-12-12', '2021-12-10', 'Test pagination', 'Gg. Ahmad Dahlan No. 236, Tarakan 25818, Babel', 'Test pagination', NULL, NULL, 9, 36.60, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(39, 1, '2021-07-07', '2021-12-12', '2021-12-10', 'Test pagination', 'Kpg. Qrisdoren No. 533, Pariaman 35949, Sumsel', 'Test pagination', NULL, NULL, 5, 36.60, 'Sehat', 'Mobil', '2021-09-22 03:43:25', '2021-09-22 03:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `presensi`
--

CREATE TABLE `presensi` (
  `id_presensi` bigint(20) UNSIGNED NOT NULL,
  `id_mhs` bigint(20) UNSIGNED NOT NULL,
  `status_presensi` tinyint(4) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `suhu_badan` double(8,2) NOT NULL,
  `kondisi_kesehatan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_jadwal` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE `prodi` (
  `id_prodi` bigint(20) UNSIGNED NOT NULL,
  `id_jurusan` bigint(20) UNSIGNED NOT NULL,
  `nama_prodi` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`id_prodi`, `id_jurusan`, `nama_prodi`, `created_at`, `updated_at`) VALUES
(1, 1, 'DIII - Teknik Konstruksi Gedung', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(2, 1, 'DIII - Teknik Konstruksi Sipil', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(3, 1, 'DIV - Teknik Perancangan Jalan dan Jembatan', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(4, 1, 'DIV - Teknik Perawatan dan Perbaikan Gedung', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(5, 2, 'DIII - Teknik Mesin', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(6, 2, 'DIII - Teknik Aeronautika', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(7, 2, 'DIV - Teknik Perancangan dan Konstruksi Mesin', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(8, 2, 'DIV - Proses Manufaktur', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(9, 3, 'DIII - Teknik Pendingin dan Tata Udara', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(10, 3, 'DIV - Teknik Pendingin dan Tata Udara', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(11, 4, 'DIII - Teknik Konversi Energi', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(12, 4, 'DIV - Teknologi Pembangkit Tenaga Listrik', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(13, 4, 'DIV - Teknik Konversi Energi', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(14, 5, 'DIII - Teknik Elektronika', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(15, 5, 'DIII - Teknik Listrik', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(16, 5, 'DIII - Teknik Telekomunikasi', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(17, 5, 'DIV - Teknik Elektronika', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(18, 5, 'DIV - Teknik Telekomunikasi', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(19, 5, 'DIV - Teknik Otomasi Indurstri', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(20, 6, 'DIII - Teknik Kimia', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(21, 6, 'DIII - Analis Kimia', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(22, 6, 'DIV - Teknik Kimia Produksi Bersih', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(23, 7, 'DIII - Teknik Informatika', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(24, 7, 'DIV - Teknik Informatika', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(25, 8, 'DIII - Akuntansi', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(26, 8, 'DIII - Keuangan dan Perbankan', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(27, 8, 'DIV - Akuntansi Manajemen Pemerintahan', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(28, 8, 'DIV - Keuangan Syariah', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(29, 8, 'DIV - Akuntasi', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(30, 9, 'DIII - Administrasi Bisnis', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(31, 9, 'DIII - Manajemen Pemasaran', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(32, 9, 'DIII - Usaha Perjalanan Wisata', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(33, 9, 'DIV - Manajemen Pemasaran', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(34, 9, 'DIV - Administrasi Bisnis', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(35, 9, 'DIV - Manajemen Aset', '2021-09-22 03:43:19', '2021-09-22 03:43:19'),
(36, 10, 'DIII - Bahasa Inggris', '2021-09-22 03:43:19', '2021-09-22 03:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `resign`
--

CREATE TABLE `resign` (
  `id_resign` bigint(20) UNSIGNED NOT NULL,
  `id_mhs` bigint(20) UNSIGNED NOT NULL,
  `tanggal_resign` date NOT NULL,
  `keterangan_resign` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suhu_badan` double(8,2) NOT NULL,
  `kondisi_kesehatan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kendaraan` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan_stnk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_resign` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resign`
--

INSERT INTO `resign` (`id_resign`, `id_mhs`, `tanggal_resign`, `keterangan_resign`, `suhu_badan`, `kondisi_kesehatan`, `jenis_kendaraan`, `keterangan_stnk`, `status_resign`, `created_at`, `updated_at`) VALUES
(1, 31, '2021-12-21', 'Masa tinggal habis', 37.10, 'Sakit', 'Sepeda', NULL, 1, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(2, 32, '2021-12-21', 'Masa tinggal habis', 37.10, 'Sehat', 'Sepeda', NULL, 1, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(3, 33, '2021-12-21', 'Masa tinggal habis', 36.70, 'Sehat', 'Sepeda', NULL, 2, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(4, 34, '2021-12-21', 'Masa tinggal habis', 37.30, 'Sakit', 'Sepeda', NULL, 3, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(5, 35, '2021-12-21', 'Masa tinggal habis', 37.00, 'Sakit', 'Sepeda', NULL, 1, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(6, 36, '2021-12-21', 'Masa tinggal habis', 37.50, 'Sakit', 'Sepeda', NULL, 0, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(7, 37, '2021-12-21', 'Masa tinggal habis', 37.00, 'Sakit', 'Sepeda', NULL, 4, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(8, 38, '2021-12-21', 'Masa tinggal habis', 37.30, 'Sehat', 'Sepeda', NULL, 0, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(9, 39, '2021-12-21', 'Masa tinggal habis', 36.60, 'Sakit', 'Sepeda', NULL, 1, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(10, 40, '2021-12-21', 'Masa tinggal habis', 36.80, 'Sehat', 'Sepeda', NULL, 3, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(11, 41, '2021-12-21', 'Masa tinggal habis', 37.00, 'Sakit', 'Sepeda', NULL, 0, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(12, 42, '2021-12-21', 'Masa tinggal habis', 37.10, 'Sehat', 'Sepeda', NULL, 3, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(13, 43, '2021-12-21', 'Masa tinggal habis', 37.40, 'Sakit', 'Sepeda', NULL, 4, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(14, 44, '2021-12-21', 'Masa tinggal habis', 36.80, 'Sakit', 'Sepeda', NULL, 0, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(15, 45, '2021-12-21', 'Masa tinggal habis', 37.50, 'Sehat', 'Sepeda', NULL, 4, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(16, 1, '2021-12-21', 'Test pagination', 37.40, 'Sakit', 'Sepeda', NULL, 0, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(17, 1, '2021-12-21', 'Test pagination', 37.30, 'Sehat', 'Sepeda', NULL, 1, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(18, 1, '2021-12-21', 'Test pagination', 37.50, 'Sakit', 'Sepeda', NULL, 4, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(19, 1, '2021-12-21', 'Test pagination', 37.00, 'Sakit', 'Sepeda', NULL, 0, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(20, 1, '2021-12-21', 'Test pagination', 37.30, 'Sehat', 'Sepeda', NULL, 2, '2021-09-22 03:43:25', '2021-09-22 03:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_users` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_users`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ichwan.latif.tif18@polban.ac.id', '$2y$10$ihbkdUXwrxUovsLIzoAnSOuOKNhLLtkCBUfX5qarUfHsghoS2CVEG', 1, NULL, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(2, 'mahasiswa1@polban.ac.id', '$2y$10$KVtePTzf7XyrlwM/I6Z6ueDEou1w1vEt/agrMT3fQwWkP261I3Dne', 1, NULL, '2021-09-22 03:43:20', '2021-09-22 03:43:20'),
(3, 'mahasiswa2@polban.ac.id', '$2y$10$jX6/mUUaj7q22IejUtQ0kuNbvpymj0p6MbW/esUCOrG9H6isFyj8u', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(4, 'mahasiswa3@polban.ac.id', '$2y$10$lC2ogE5yS4KlG3SELNzUx.m5ZbgT83DCWRar53QqlFJWTAp9iyRdy', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(5, 'mahasiswa4@polban.ac.id', '$2y$10$QanRZR.Ij3TXTM2GEE3JdePm/oJYd1rbURlD0xo/eH/JHk2WW8HeS', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(6, 'mahasiswa5@polban.ac.id', '$2y$10$N2zwWi81/7vgM1egBF4ZFeQ/Mj1KF7acP77zLMXzsVzQgPTAx.aIC', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(7, 'mahasiswa6@polban.ac.id', '$2y$10$xu03OZ9JuTOO8i6bTUAstOYRtLzEyjrwIbUq.L9xShwz.Ddz/glL6', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(8, 'mahasiswa7@polban.ac.id', '$2y$10$kVS2VpRgkl6aEhhsuh7aieymufriC12LBv90hGqZ89Z6J6SbcKcrO', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(9, 'mahasiswa8@polban.ac.id', '$2y$10$uxQHHalQW3EwNz1cp7MAZusDQsinTsDuyi4yCJhm5Psn8yfy2n9sW', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(10, 'mahasiswa9@polban.ac.id', '$2y$10$TYNjxTglU9BYs0pZwzckBurmYwmV9rtqgrhJhlIDGrshas.jCvZ4S', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(11, 'mahasiswa10@polban.ac.id', '$2y$10$pow88XH0OxwbdVNby362quYHfaqQg4OB207fvytSCDu9zVAbEY5WW', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(12, 'mahasiswa11@polban.ac.id', '$2y$10$/M2BQqmINLRHjk3Lb6bLmOgCSX4RLjCwO0MxvKvpbmNszL5BodS5y', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(13, 'mahasiswa12@polban.ac.id', '$2y$10$kFFL3CV2m9pj9vfTVLbYeu9Gq7M1yn7CKhtjdPVH7wjmLkUs6DDWO', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(14, 'mahasiswa13@polban.ac.id', '$2y$10$jl6EZdlYU1JWL.dPqE88jupaWbYS3dGsFctT8fZp2VZd2fIl0J.ji', 1, NULL, '2021-09-22 03:43:21', '2021-09-22 03:43:21'),
(15, 'mahasiswa14@polban.ac.id', '$2y$10$wSoOj2xz73ovC8cjBu5mu.pY93ZU8pMBMgzuf0u061VSLUUaBtSye', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(16, 'mahasiswa15@polban.ac.id', '$2y$10$JUTTqbbwb8fMj0jUCC12vOrbyqrcxKyeQDKoZF2QTwz42V.TSM652', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(17, 'mahasiswa16@polban.ac.id', '$2y$10$R1xdFmkg3dAA.T7Gxrb7Vu0N0WJ/4/NTPNsf71JBJhaFmwL9dIRMC', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(18, 'mahasiswa17@polban.ac.id', '$2y$10$Ek7sMjFU5v0cW/SVp3wRo.IApw8lOwDtoKbFFvfhu.6dpkjEi28rW', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(19, 'mahasiswa18@polban.ac.id', '$2y$10$JiAx09omk4lHoy0au2oqwOjLnYjfb1s9soBXb7ST5TgMjQhqyfYAW', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(20, 'mahasiswa19@polban.ac.id', '$2y$10$3y8U86HLSuV1Ng4tdOqgO.6bjwWvd.NpAqQfW9fyyjudR5KMDGql2', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(21, 'mahasiswa20@polban.ac.id', '$2y$10$7paWQi4pZ4X5mbCQPOBgI.80IeaRQL6O3u6wNMz573SMc/6wintha', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(22, 'mahasiswa21@polban.ac.id', '$2y$10$KH0amNK85/siuiV3QRPKJO.Dju4PjtVZgBClUmnsB0NSZDwmVFa66', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(23, 'mahasiswa22@polban.ac.id', '$2y$10$e/NygG9zParyxqPjKhVDVu0QDDDCq1eh/5e55oZdWW2eW2LP51iQu', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(24, 'mahasiswa23@polban.ac.id', '$2y$10$CTtmGjyoq7YOPQUePlPQGOInBMXUGuA.j3pO1QpFwAUHPiAEApx3G', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(25, 'mahasiswa24@polban.ac.id', '$2y$10$pHeulzZAXHHZgokFmddA7uja.o2Hdezp94MLAv1UP.vgycChZXG1q', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(26, 'mahasiswa25@polban.ac.id', '$2y$10$068xlMOu4fUbLaaS7otCSe3zzqu4Fvdhz16lahEgsqJHzfWWHHDbq', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(27, 'mahasiswa26@polban.ac.id', '$2y$10$G1TN4q2tHZsaZYMox/Ggd.DzT1lT7s7bB74ALKhlyyxtZiqWvcDC2', 1, NULL, '2021-09-22 03:43:22', '2021-09-22 03:43:22'),
(28, 'mahasiswa27@polban.ac.id', '$2y$10$W4GELSyDoauEAtPfKcA.lu.nPynZqcC2gDGS4NGpdpNpJ7lXvk.bm', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(29, 'mahasiswa28@polban.ac.id', '$2y$10$z5YV1LjaQ.lGF3YwJe8DrenMWncsJo2wqYGc7vw6zgmf4Cpe5Qnuq', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(30, 'mahasiswa29@polban.ac.id', '$2y$10$mCQw2xAy98Vxmm5JSDW6vuggiya6ldH0Qa9hzx4udQ9uKrAdoZ2L6', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(31, 'mahasiswa30@polban.ac.id', '$2y$10$nwITywMlQDE5CsspXVuDR.OoHLWZpKlPxlWMvs93vpKxUHdy/o4/W', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(32, 'mahasiswa31@polban.ac.id', '$2y$10$7f1E6wkMejhAXhIckw5Fv.Be8BAqIjudS6eT7TAlKEvbzZ.3eQvN6', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(33, 'mahasiswa32@polban.ac.id', '$2y$10$sTeA5vS99GdZtsOvNKKdFOz3TYK0Dx1Fd7mWuSRN7Hy35JybU5MIa', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(34, 'mahasiswa33@polban.ac.id', '$2y$10$bQOMjNAy/HADXtGkCZWkTeuDNhthTNXcJ7uWNoKY6lgzlU09Ytt7C', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(35, 'mahasiswa34@polban.ac.id', '$2y$10$nbOF24ZuX.TgtAzR1ydtU.DzUPjCJc3Ns2.bVGat6GdgKSS2/S8ZO', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(36, 'mahasiswa35@polban.ac.id', '$2y$10$.VSGLgOh3kAJHcLBZtnOtOUIQtwvvx0eDbMIAfrtkUG8NuFHU.jo6', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(37, 'mahasiswa36@polban.ac.id', '$2y$10$KSxJ2HheH67uhlOAZBuhyOoW9Mtxa3ZExkGZUKHWnpZ2rXbNpOLEG', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(38, 'mahasiswa37@polban.ac.id', '$2y$10$S/GbPOfYAVriyCDY2cupKejqjHyN9HLCo4WFHpNq9eZa58tnoYEMe', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(39, 'mahasiswa38@polban.ac.id', '$2y$10$DRM40RfnJKTnEhkNIMoRZuqHCvrYh/gixBznffwjJ5PGR6cxouSgi', 1, NULL, '2021-09-22 03:43:23', '2021-09-22 03:43:23'),
(40, 'mahasiswa39@polban.ac.id', '$2y$10$U8UFnLSvQWALBCFNR7TfVuAItOMk6ma01qi6OESkIVio3ltnF3n1W', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(41, 'mahasiswa40@polban.ac.id', '$2y$10$X4y623cSnp5w9sTiZVxfH.578iMrDp8tbpjTuvHRekrYaceHw6ouy', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(42, 'mahasiswa41@polban.ac.id', '$2y$10$Np7ON8.11t6Q4wqR.7hAY.W3Y/7Kny8YtuYnsmM.n.6ZQ1mnW4CNy', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(43, 'mahasiswa42@polban.ac.id', '$2y$10$h/PnZgCNE8NOVa0I0eARue65UpFU/30mw8ZiEQGreQ2iN4TSGKBKW', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(44, 'mahasiswa43@polban.ac.id', '$2y$10$dX2EpPXODOImsHvrfkR2xeC/9J3pj8as2ALr70FB6nzaUnZidTsey', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(45, 'mahasiswa44@polban.ac.id', '$2y$10$sQGTOiuHCnTNhrxo0zl2iepEE3rBhy/Oa0ernMOEHM.a0Vh64KkKO', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(46, 'mahasiswa45@polban.ac.id', '$2y$10$XcA2g21aV.YCRD.Y1NfvUuGrDhE0ktGniYXopVf6fMiwjwEQ.yOcC', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(47, 'mahasiswa46@polban.ac.id', '$2y$10$Stcbx39XtB8BWV4eOQY0iucE7TSuC2Vf6f37TgnVQCF8dbCrmVSxW', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(48, 'mahasiswa47@polban.ac.id', '$2y$10$NW/TDYPc4Eu2aAyfgKpIEOdusiwHajyTWS3pm2fOCFMUmpgjSL1pW', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(49, 'mahasiswa48@polban.ac.id', '$2y$10$awyCPR4nPiRRNYRdtKWdpeHIjVKdcVNa4odCiqnxWK2MPN3wL1xX6', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(50, 'mahasiswa49@polban.ac.id', '$2y$10$7.9BDEPloxrTqtsSKB0SJeLBX8YU9v6ciyk8i5eYVPLQi34qZ.dy2', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(51, 'mahasiswa50@polban.ac.id', '$2y$10$wHJ77U0aJ33Lo.tOf8/E1unuo2OjfDAAbGOWAnwk7MWNcvqiKyeJu', 1, NULL, '2021-09-22 03:43:24', '2021-09-22 03:43:24'),
(52, 'irfan.siswara.tif18@polban.ac.id', '$2y$10$7XWcmz1GZBJ6i6kRR.IDSurH3MFiI5BX2JWXs/imzZZjn.y1nFcQy', 3, NULL, '2021-09-22 03:43:25', '2021-09-22 03:43:25'),
(53, 'rizqa.fauziyyah.tif18@polban.ac.id', '$2y$10$4KueWLDyaMoIJr0kacvWOOlZL0xJYDdIjozIZwMdO6Am2aAg0VTVa', 2, NULL, '2021-09-22 03:43:25', '2021-09-22 03:43:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `gedung`
--
ALTER TABLE `gedung`
  ADD PRIMARY KEY (`id_gedung`);

--
-- Indexes for table `jadwal_presensi`
--
ALTER TABLE `jadwal_presensi`
  ADD PRIMARY KEY (`id_jadwal_presensi`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `kamar`
--
ALTER TABLE `kamar`
  ADD PRIMARY KEY (`id_kamar`),
  ADD KEY `kamar_id_gedung_foreign` (`id_gedung`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mhs`),
  ADD KEY `mahasiswa_id_users_foreign` (`id_users`),
  ADD KEY `mahasiswa_id_prodi_foreign` (`id_prodi`),
  ADD KEY `mahasiswa_id_kamar_foreign` (`id_kamar`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pengelola`
--
ALTER TABLE `pengelola`
  ADD PRIMARY KEY (`id_pengelola`),
  ADD KEY `pengelola_id_users_foreign` (`id_users`);

--
-- Indexes for table `perizinan`
--
ALTER TABLE `perizinan`
  ADD PRIMARY KEY (`id_perizinan`),
  ADD KEY `perizinan_id_mhs_foreign` (`id_mhs`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `presensi`
--
ALTER TABLE `presensi`
  ADD PRIMARY KEY (`id_presensi`),
  ADD KEY `presensi_id_mhs_foreign` (`id_mhs`),
  ADD KEY `presensi_id_jadwal_foreign` (`id_jadwal`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id_prodi`),
  ADD KEY `prodi_id_jurusan_foreign` (`id_jurusan`);

--
-- Indexes for table `resign`
--
ALTER TABLE `resign`
  ADD PRIMARY KEY (`id_resign`),
  ADD KEY `resign_id_mhs_foreign` (`id_mhs`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gedung`
--
ALTER TABLE `gedung`
  MODIFY `id_gedung` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jadwal_presensi`
--
ALTER TABLE `jadwal_presensi`
  MODIFY `id_jadwal_presensi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `kamar`
--
ALTER TABLE `kamar`
  MODIFY `id_kamar` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id_mhs` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pengelola`
--
ALTER TABLE `pengelola`
  MODIFY `id_pengelola` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `perizinan`
--
ALTER TABLE `perizinan`
  MODIFY `id_perizinan` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `presensi`
--
ALTER TABLE `presensi`
  MODIFY `id_presensi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prodi`
--
ALTER TABLE `prodi`
  MODIFY `id_prodi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `resign`
--
ALTER TABLE `resign`
  MODIFY `id_resign` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_users` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kamar`
--
ALTER TABLE `kamar`
  ADD CONSTRAINT `kamar_id_gedung_foreign` FOREIGN KEY (`id_gedung`) REFERENCES `gedung` (`id_gedung`) ON DELETE CASCADE;

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_id_kamar_foreign` FOREIGN KEY (`id_kamar`) REFERENCES `kamar` (`id_kamar`) ON DELETE CASCADE,
  ADD CONSTRAINT `mahasiswa_id_prodi_foreign` FOREIGN KEY (`id_prodi`) REFERENCES `prodi` (`id_prodi`) ON DELETE CASCADE,
  ADD CONSTRAINT `mahasiswa_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Constraints for table `pengelola`
--
ALTER TABLE `pengelola`
  ADD CONSTRAINT `pengelola_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Constraints for table `perizinan`
--
ALTER TABLE `perizinan`
  ADD CONSTRAINT `perizinan_id_mhs_foreign` FOREIGN KEY (`id_mhs`) REFERENCES `mahasiswa` (`id_mhs`) ON DELETE CASCADE;

--
-- Constraints for table `presensi`
--
ALTER TABLE `presensi`
  ADD CONSTRAINT `presensi_id_jadwal_foreign` FOREIGN KEY (`id_jadwal`) REFERENCES `jadwal_presensi` (`id_jadwal_presensi`),
  ADD CONSTRAINT `presensi_id_mhs_foreign` FOREIGN KEY (`id_mhs`) REFERENCES `mahasiswa` (`id_mhs`) ON DELETE CASCADE;

--
-- Constraints for table `prodi`
--
ALTER TABLE `prodi`
  ADD CONSTRAINT `prodi_id_jurusan_foreign` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id_jurusan`) ON DELETE CASCADE;

--
-- Constraints for table `resign`
--
ALTER TABLE `resign`
  ADD CONSTRAINT `resign_id_mhs_foreign` FOREIGN KEY (`id_mhs`) REFERENCES `mahasiswa` (`id_mhs`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
